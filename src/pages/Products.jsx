import React, { useEffect, useState } from "react";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import Paper from "@mui/material/Paper";
import Box from "@mui/material/Box";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Avatar from "@mui/material/Avatar";
import { Link } from "react-router-dom";
import { createTheme, useTheme, ThemeProvider } from "@mui/material/styles";
import Grid from "@mui/material/Grid";
import { styled } from "@mui/material/styles";
import PropTypes from "prop-types";
import TextField from "@mui/material/TextField";
import IconButton from "@mui/material/IconButton";
import TableFooter from "@mui/material/TableFooter";
import TablePagination from "@mui/material/TablePagination";

import Appbar from "../components/appbar";

import Swal from "sweetalert2";

//import icons
import DeleteForeverRoundedIcon from "@mui/icons-material/DeleteForeverRounded";
import AddIcon from "@mui/icons-material/Add";
import ClearIcon from "@mui/icons-material/Clear";
import SearchIcon from "@mui/icons-material/Search";
import FirstPageIcon from "@mui/icons-material/FirstPage";
import KeyboardArrowLeft from "@mui/icons-material/KeyboardArrowLeft";
import KeyboardArrowRight from "@mui/icons-material/KeyboardArrowRight";
import LastPageIcon from "@mui/icons-material/LastPage";
import ModeEditIcon from "@mui/icons-material/ModeEdit";
import CheckroomIcon from "@mui/icons-material/Checkroom";

function TablePaginationActions(props) {
  const theme = useTheme();
  const { count, page, rowsPerPage, onPageChange } = props;

  const handleFirstPageButtonClick = (event) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <Box sx={{ flexShrink: 0, ml: 2.5, border: 0 }}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowRight />
        ) : (
          <KeyboardArrowLeft />
        )}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowLeft />
        ) : (
          <KeyboardArrowRight />
        )}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </Box>
  );
}

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};

export default function Products() {
  const [platform, setPlatform] = useState([]);
  const [searchText, setSearchText] = useState("");
  const [rows] = useState([]);
  const [products, setProduct] = useState([]);
  useEffect(() => {
    ProductGet();
  }, []);

  const ProductGet = () => {
    fetch("https://crud-wms.herokuapp.com/pd")
      .then((res) => res.json())
      .then((rows) => {
        setProduct(rows);
        setPlatform(rows);
      });
  };

  function escapeRegExp(value) {
    return value.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
  }

  const requestSearch = (searchValue) => {
    const searchRegex = new RegExp(escapeRegExp(searchValue), "i");
    const filteredProducts = platform.filter((products) => {
      return Object.keys(products).some((field) => {
        return searchRegex.test(products[field]);
      });
    });
    setProduct(filteredProducts);
  };

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const UpdateProduct = (product_id) => {
    window.location = "/Updatepd/" + product_id;
  };

  const ProductDelete = (product_id) => {
    var data = {
      product_id: product_id,
    };
    fetch("https://crud-wms.herokuapp.com/deletepd", {
      method: "DELETE",
      headers: {
        Accept: "application/form-data",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((res) => res.json())
      .then((rows) => {
        if (rows["status"] === "ok") {
          Swal.fire({
            icon: "success",
            title: "Deleted!",
            text: rows["message"],
            toast: true,
            position: "top-end",
            showConfirmButton: false,
            timer: 2500,
            timerProgressBar: true,
          });
          ProductGet();
          window.location = "/Products";
        }
      });
  };

  const mdTheme = createTheme({
    palette: {
      primary: {
        main: "#001345",
        info: "#e7e8fc",
      },
      secondary: {
        main: "#878787",
        info: "#ffffff",
      },
      neutral: {
        main: "#ffffff",
        contrastText: "#fff",
      },
    },
  });

  const role = JSON.parse(localStorage.getItem("user"));

  const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.primary.main,
      color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 18,
    },
  }));

  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    "&:last-child td, &:last-child th": {
      border: 0,
    },
  }));

  if (role.role_id === 1) {
    return (
      <ThemeProvider theme={mdTheme}>
        <Grid container spacing={1}>
          <Box
            sx={{
              border: 0,
              bgcolor: "primary.info",
              width: "100%",
              height: "auto",
              minHeight: 890,
              borderLeft: 71,
              borderColor: "primary.main",
            }}
          >
            <Grid item xs={12}>
              <Appbar />
              <Grid>
                <Paper sx={{ p: 3, m: 5, mt: 15 }}>
                  <Box display="flex">
                    <Box flexGrow={1}>
                      <Typography
                        component="h2"
                        variant="h6"
                        color="primary.main"
                        gutterBottom
                        sx={{ ml: 3, mt: 1 }}
                      >
                        PRODUCTS
                      </Typography>
                    </Box>

                    <Box
                      display="flex"
                      sx={{ justifyContent: "flex-end", mr: 5 }}
                    >
                      <TextField
                        variant="standard"
                        value={searchText}
                        onChange={(e) => {
                          setSearchText(e.target.value);
                          requestSearch(e.target.value);
                        }}
                        placeholder="Search..."
                        InputProps={{
                          startAdornment: (
                            <SearchIcon fontSize="medium" color="action" />
                          ),
                          endAdornment: (
                            <IconButton
                              title="Clear"
                              aria-label="Clear"
                              size="small"
                              style={{
                                visibility: searchText ? "visible" : "hidden",
                                borderRadius: "57%",
                                paddingRight: "1px",
                                margin: "0",
                                fontSize: "1.25rem",
                              }}
                              onClick={(e) => {
                                setSearchText("");
                                setProduct(platform);
                              }}
                            >
                              <ClearIcon fontSize="medium" color="action" />
                            </IconButton>
                          ),
                        }}
                        sx={{
                          width: { xs: 1, sm: "auto" },
                          m: (theme) => theme.spacing(1, 0.5, 1.5),
                          "& .MuiSvgIcon-root": {
                            mr: 0.5,
                          },
                          "& .MuiInput-underline:before": {
                            borderBottom: 1,
                            borderColor: "divider",
                          },
                        }}
                      />
                    </Box>
                    <Box>
                      <Link to="/ProductRegister">
                        <Button
                          variant="contained"
                          color="primary"
                          sx={{ width: 150, height: 50, mr: 5 }}
                        >
                          <AddIcon sx={{ mr: 1 }} />
                          CREATE
                        </Button>
                      </Link>
                    </Box>
                  </Box>
                  <TableContainer component={Paper} sx={{ mt: 2 }}>
                    <Table aria-label="customized table">
                      <TableHead>
                        <StyledTableRow>
                          <StyledTableCell align="center" width="5%">
                            ID
                          </StyledTableCell>
                          <StyledTableCell align="center" width="10%">
                            Products's Name
                          </StyledTableCell>
                          <StyledTableCell align="center" width="10%">
                            Picture
                          </StyledTableCell>
                          <StyledTableCell align="left" width="30%">
                            Description
                          </StyledTableCell>
                          <StyledTableCell align="center" width="15%">
                            Quantity
                          </StyledTableCell>
                          <StyledTableCell align="center" width="10%">
                            Action
                          </StyledTableCell>
                        </StyledTableRow>
                      </TableHead>
                      <TableBody>
                        {(rowsPerPage > 0
                          ? products.slice(
                              page * rowsPerPage,
                              page * rowsPerPage + rowsPerPage
                            )
                          : products
                        ).map((product) => (
                          <StyledTableRow key={product.product_id}>
                            <StyledTableCell align="center">
                              {product.product_id}
                            </StyledTableCell>

                            <StyledTableCell align="center">
                              {product.product_name}
                            </StyledTableCell>
                            <StyledTableCell align="center">
                              <Box display="flex" justifyContent="center">
                                <Avatar
                                  src={product.product_picture}
                                  sx={{ width: 150, height: 150 }}
                                />
                              </Box>
                            </StyledTableCell>
                            <StyledTableCell align="left">
                              {product.description}
                            </StyledTableCell>
                            <StyledTableCell align="center">
                              {product.Quantity}
                            </StyledTableCell>
                            <StyledTableCell align="center">
                              <Button
                                color="primary"
                                variant="contained"
                                sx={{
                                  borderRadius: 2,
                                  mb: 0.5,
                                  width: 120,
                                }}
                                onClick={() =>
                                  UpdateProduct(product.product_id)
                                }
                              >
                                <ModeEditIcon sx={{ mr: 1 }} />
                                Edit
                              </Button>
                              <Button
                                color="error"
                                variant="contained"
                                size="small"
                                onClick={() =>
                                  ProductDelete(product.product_id)
                                }
                                sx={{
                                  borderRadius: 2,
                                  width: 120,
                                }}
                              >
                                <DeleteForeverRoundedIcon sx={{ mr: 1 }} />
                                delete
                              </Button>
                            </StyledTableCell>
                          </StyledTableRow>
                        ))}
                      </TableBody>
                      <TableFooter>
                        <TableRow sx={{ borderTop: 0 }}>
                          <TablePagination
                            rowsPerPageOptions={[
                              10,
                              25,
                              50,
                              100,
                              { label: "All", value: -1 },
                            ]}
                            colSpan={4}
                            count={products.length}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            SelectProps={{
                              inputProps: {
                                "aria-label": "rows per page",
                              },
                              native: true,
                            }}
                            onPageChange={handleChangePage}
                            onRowsPerPageChange={handleChangeRowsPerPage}
                            ActionsComponent={TablePaginationActions}
                          />
                        </TableRow>
                      </TableFooter>
                    </Table>
                  </TableContainer>
                </Paper>
              </Grid>
            </Grid>
          </Box>
        </Grid>
      </ThemeProvider>
    );
  } else {
    return (
      <ThemeProvider theme={mdTheme}>
        <Grid container spacing={1}>
          <Box
            sx={{
              border: 0,
              bgcolor: "primary.info",
              width: "100%",
              height: "auto",
              minHeight: 890,
              borderLeft: 71,
              borderColor: "primary.main",
            }}
          >
            <Grid item xs={12}>
              <Appbar />
              <Grid>
                <Paper sx={{ p: 3, m: 5, mt: 15 }}>
                  <Box display="flex">
                    <Box flexGrow={1}>
                      <Typography
                        component="h2"
                        variant="h6"
                        color="primary.main"
                        gutterBottom
                        sx={{ ml: 3, mt: 1 }}
                      >
                        PRODUCTS
                      </Typography>
                    </Box>

                    <Box
                      display="flex"
                      sx={{ justifyContent: "flex-end", mr: 5 }}
                    >
                      <TextField
                        variant="standard"
                        value={searchText}
                        onChange={(e) => {
                          setSearchText(e.target.value);
                          requestSearch(e.target.value);
                        }}
                        placeholder="Search..."
                        InputProps={{
                          startAdornment: (
                            <SearchIcon fontSize="medium" color="action" />
                          ),
                          endAdornment: (
                            <IconButton
                              title="Clear"
                              aria-label="Clear"
                              size="small"
                              style={{
                                visibility: searchText ? "visible" : "hidden",
                                borderRadius: "57%",
                                paddingRight: "1px",
                                margin: "0",
                                fontSize: "1.25rem",
                              }}
                              onClick={(e) => {
                                setSearchText("");
                                setProduct(platform);
                              }}
                            >
                              <ClearIcon fontSize="medium" color="action" />
                            </IconButton>
                          ),
                        }}
                        sx={{
                          width: { xs: 1, sm: "auto" },
                          m: (theme) => theme.spacing(1, 0.5, 1.5),
                          "& .MuiSvgIcon-root": {
                            mr: 0.5,
                          },
                          "& .MuiInput-underline:before": {
                            borderBottom: 1,
                            borderColor: "divider",
                          },
                        }}
                      />
                    </Box>
                  </Box>
                  <TableContainer component={Paper} sx={{ mt: 2 }}>
                    <Table aria-label="customized table">
                      <TableHead>
                        <StyledTableRow>
                          <StyledTableCell align="center" width="10%">
                            ID
                          </StyledTableCell>
                          <StyledTableCell align="center" width="10%">
                            Products's Name
                          </StyledTableCell>
                          <StyledTableCell align="center">
                            Picture
                          </StyledTableCell>
                          <StyledTableCell align="left" width="30%">
                            Description
                          </StyledTableCell>
                          <StyledTableCell align="center" width="10%">
                            Quantity
                          </StyledTableCell>
                        </StyledTableRow>
                      </TableHead>
                      <TableBody>
                        {(rowsPerPage > 0
                          ? products.slice(
                              page * rowsPerPage,
                              page * rowsPerPage + rowsPerPage
                            )
                          : products
                        ).map((product) => (
                          <StyledTableRow key={product.product_id}>
                            <StyledTableCell align="center">
                              {product.product_id}
                            </StyledTableCell>

                            <StyledTableCell align="left">
                              {product.product_name}
                            </StyledTableCell>
                            <StyledTableCell align="center">
                              <Box display="flex" justifyContent="center">
                                <Avatar
                                  src={product.product_picture}
                                  sx={{ width: 150, height: 150 }}
                                />
                              </Box>
                            </StyledTableCell>
                            <StyledTableCell align="left">
                              {product.description}
                            </StyledTableCell>
                            <StyledTableCell align="center">
                              {product.Quantity}
                            </StyledTableCell>
                          </StyledTableRow>
                        ))}
                      </TableBody>
                      <TableFooter>
                        <TableRow>
                          <TablePagination
                            rowsPerPageOptions={[
                              10,
                              25,
                              50,
                              100,
                              { label: "All", value: -1 },
                            ]}
                            colspan={4}
                            count={products.length}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            SelectProps={{
                              inputProps: {
                                "aria-label": "rows per page",
                              },
                              native: true,
                            }}
                            onPageChange={handleChangePage}
                            onRowsPerPageChange={handleChangeRowsPerPage}
                            ActionsComponent={TablePaginationActions}
                          />
                        </TableRow>
                      </TableFooter>
                    </Table>
                  </TableContainer>
                </Paper>
              </Grid>
            </Grid>
          </Box>
        </Grid>
      </ThemeProvider>
    );
  }
}
