import React, { useEffect, useState } from "react";
import { motion } from "framer-motion";

// MUI import
import { createTheme, ThemeProvider } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Stack from "@mui/material/Stack";

//import component
import Appbar from "../components/appbar";
import Chart from "../components/Dashboard page/Chart";
import Product from "../components/Dashboard page/Product";
import Stock from "../components/Dashboard page/Stock";
import Exported from "../components/Dashboard page/Exported";
import Remaining from "../components/Dashboard page/Remaining";

const mdTheme = createTheme({
  palette: {
    primary: {
      main: "#001345",
      info: "#e7e8fc",
    },
    secondary: {
      main: "#878787",
    },
  },
});

function DashboardContent() {
  const [anchorElNav, setAnchorElNav] = React.useState(null);
  const [anchorElUser, setAnchorElUser] = React.useState(null);

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
    >
      <ThemeProvider theme={mdTheme}>
        <CssBaseline />
        <Grid container spacing={18} sx={{ border: 0 }}>
          <Grid item xs={12}>
            <Box
              sx={{
                bgcolor: "primary.info",
                width: "auto",
                height: "100%",
                borderLeft: 71,
                borderColor: "primary.main",
              }}
            >
              <Appbar />
              <Container maxWidth="xlg" sx={{ pt: 9, mt: 5, mb: 4, border: 0 }}>
                <Grid container>
                  {/* Fluid grids */}
                  <Grid item xs={3} sx={{ border: 0 }}>
                    <Box
                      sx={{
                        pl: 2,
                        alignItems: "center",
                        display: "flex",
                        flexDirection: "column",
                      }}
                    >
                      {/* Tags Counted */}
                      <Paper
                        elevation="5"
                        sx={{
                          p: 2,
                          mb: 2,
                          display: "flex",
                          borderRadius: "11px",
                          flexDirection: "column",
                          height: 200,
                          width: 286,
                        }}
                      >
                        <Typography
                          sx={{
                            justifyContent: "flex-start",
                            fontSize: "20px",
                            pb: 3,
                          }}
                        >
                          Registered tags
                        </Typography>
                        <Product />
                      </Paper>

                      {/* Tags Counted */}

                      <Paper
                        elevation="5"
                        sx={{
                          p: 2,
                          mb: 2,
                          display: "flex",
                          borderRadius: "11px",
                          flexDirection: "column",
                          height: 140,
                          width: 286,
                        }}
                      >
                        <Typography
                          sx={{
                            justifyContent: "flex-start",
                            fontSize: "18px",
                          }}
                        >
                          Today's products in stock
                        </Typography>
                        <Box sx={{ alignItems: "center" }}>
                          <Stock />
                        </Box>
                      </Paper>

                      {/* Tags Counted */}

                      <Paper
                        elevation="5"
                        sx={{
                          p: 2,
                          mb: 2,
                          display: "flex",
                          borderRadius: "11px",
                          flexDirection: "column",
                          height: 140,
                          width: 286,
                        }}
                      >
                        <Typography
                          sx={{
                            justifyContent: "flex-start",
                            fontSize: "18px",
                          }}
                        >
                          Exported
                        </Typography>
                        <Box
                          sx={{
                            mt: 1.75,
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                          }}
                        >
                          <Typography
                            component="p"
                            variant="h6"
                            sx={{ textAlign: "center", fontSize: "38px" }}
                          >
                            0
                          </Typography>
                        </Box>
                      </Paper>

                      {/* Tags Counted */}

                      <Paper
                        elevation="5"
                        sx={{
                          p: 2,
                          mb: 2,
                          display: "flex",
                          borderRadius: "11px",
                          flexDirection: "column",
                          height: 140,
                          width: 286,
                        }}
                      >
                        <Typography
                          sx={{
                            justifyContent: "flex-start",
                            fontSize: "18px",
                          }}
                        >
                          Remaining
                        </Typography>
                        <Box sx={{ alignItems: "center" }}>
                          <Stock />
                        </Box>
                      </Paper>

                      <Paper
                        elevation="5"
                        sx={{
                          p: 2,
                          mb: 1.2,
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                          borderRadius: "11px",
                          flexDirection: "column",
                          height: 150,
                          width: 292,
                        }}
                      >
                        <Typography
                          sx={{
                            justifyContent: "flex-start",
                            fontSize: "24px",
                            color: "#ffc658",
                          }}
                        >
                          amt = Remaining tags
                        </Typography>

                        <Typography
                          sx={{
                            justifyContent: "flex-start",
                            fontSize: "24px",
                            color: "#82ca9d",
                          }}
                        >
                          pv = Exported tags
                        </Typography>
                        <Typography
                          sx={{
                            justifyContent: "flex-start",
                            fontSize: "24px",
                            color: "#8884d8",
                          }}
                        >
                          uv = Total of tags
                        </Typography>
                      </Paper>
                    </Box>
                  </Grid>

                  {/* Fluid grids */}
                  <Grid container item xs={9} sx={{ display: "flex" }}>
                    {/* Chart */}

                    <Paper
                      elevation="10"
                      sx={{
                        p: 0,
                        pl: 4,
                        mr: 5,
                        display: "flex",
                        flexDirection: "column",
                        width: 500,
                        height: 300,
                        borderRadius: "11px",
                        justifyContent: "center",
                      }}
                    >
                      <Exported />
                    </Paper>

                    <Paper
                      elevation="10"
                      sx={{
                        p: 0,
                        pl: 4,
                        ml: 1,
                        mr: 5.5,
                        display: "flex",
                        flexDirection: "column",
                        width: 250,
                        height: 300,
                        borderRadius: "11px",
                        justifyContent: "center",
                      }}
                    >
                      <Stack direction="row" spacing={2}>
                        <Paper
                          elevation="3"
                          sx={{
                            height: 20,
                            width: 20,
                            bgcolor: "#ddfafb",
                            mb: 2,
                          }}
                        ></Paper>
                        <Typography> T-Shirt </Typography>
                      </Stack>

                      <Stack direction="row" spacing={2}>
                        <Paper
                          elevation="3"
                          sx={{
                            height: 20,
                            width: 20,
                            bgcolor: "#ea8bcb",
                            mb: 2,
                          }}
                        ></Paper>
                        <Typography> Polo Shirt </Typography>
                      </Stack>

                      <Stack direction="row" spacing={2}>
                        <Paper
                          elevation="3"
                          sx={{
                            height: 20,
                            width: 20,
                            bgcolor: "#fff59d",
                            mb: 2,
                          }}
                        ></Paper>
                        <Typography> Cardigan </Typography>
                      </Stack>

                      <Stack direction="row" spacing={2}>
                        <Paper
                          elevation="3"
                          sx={{
                            height: 20,
                            width: 20,
                            bgcolor: "#001345",
                            mb: 2,
                          }}
                        ></Paper>
                        <Typography> Jeans </Typography>
                      </Stack>

                      <Stack direction="row" spacing={2}>
                        <Paper
                          elevation="3"
                          sx={{ height: 20, width: 20, bgcolor: "#c5a0ff" }}
                        ></Paper>
                        <Typography> Skirt </Typography>
                      </Stack>
                    </Paper>

                    <Paper
                      elevation="10"
                      sx={{
                        p: 2,
                        display: "flex",
                        flexDirection: "column",
                        width: 457,
                        height: 300,
                        borderRadius: "11px",
                        justifyContent: "center",
                      }}
                    >
                      <Remaining />
                    </Paper>

                    {/* Recent Orders */}
                    <Paper
                      elevation="15"
                      sx={{
                        mt: 3,
                        p: 2,
                        display: "flex",
                        flexDirection: "column",
                        borderRadius: "11px",
                        width: 1300,
                        height: 500,
                      }}
                    >
                      <Chart />
                    </Paper>
                  </Grid>
                </Grid>
              </Container>
            </Box>
          </Grid>
        </Grid>
      </ThemeProvider>
    </motion.div>
  );
}

export default function Dashboard() {
  return <DashboardContent />;
}
