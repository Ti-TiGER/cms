import React, { useEffect, useState } from "react";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import Link from "@mui/material/Link";
import Paper from "@mui/material/Paper";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import InputAdornment from "@mui/material/InputAdornment";
import Stack from "@mui/material/Stack";
import IconButton from "@mui/material/IconButton";

import Swal from "sweetalert2";

//icon stuff
import PersonOutlineOutlinedIcon from "@mui/icons-material/PersonOutlineOutlined";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import CheckBoxOutlineBlankRoundedIcon from "@mui/icons-material/CheckBoxOutlineBlankRounded";
import CheckBoxIcon from "@mui/icons-material/CheckBox";
import VisibilityIcon from "@mui/icons-material/Visibility";
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff";

export default function Login() {
  const handleSubmit = (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    const jsonData = {
      email: data.get("email"),
      password: data.get("password"),
      rememberme: false,
    };

    fetch("https://crud-wms.herokuapp.com/login", {
      method: "POST",
      headers: {
        Accept: "application/form-data",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(jsonData),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data["status"] === "success") {
          Swal.fire({
            icon: "success",
            title: "Login!",
            text: data["message"],
            toast: true,
            position: "top-end",
            showConfirmButton: false,
            timer: 1500,
            timerProgressBar: true,
          });
          localStorage.setItem("token", data.token);
          localStorage.setItem("user", JSON.stringify(data.user));
          window.setTimeout(function () {
            window.location.href = "/Home";
          }, 1500);
        } else if (data["status"] === "Invalid password") {
          Swal.fire({
            icon: "error",
            title: "Login fail",
            text: data["message"],
            toast: true,
            position: "top-end",
            showConfirmButton: false,
            timer: 2000,
            timerProgressBar: true,
          });
        }
      })
      .catch((error) => console.log("error", error));
  };

  const [values, setValues] = React.useState({
    password: "",
    showPassword: false,
  });

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const handlePasswordChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value });
  };

  const theme = createTheme({
    palette: {
      primary: {
        main: "#142454",
      },
    },
  });

  const avatarStyle = { backgroundColor: "#041444" };

  return (
    <ThemeProvider theme={theme}>
      <Grid container component="main" sx={{ height: "100vh" }}>
        <CssBaseline />
        <Grid
          item
          xs={false}
          sm={4}
          md={7}
          sx={{
            backgroundImage:
              "url(https://cdn.discordapp.com/attachments/744696824220221502/984030671427231775/unknown.png)",
            backgroundRepeat: "no-repeat",
            backgroundColor: (t) =>
              t.palette.mode === "light"
                ? t.palette.grey[50]
                : t.palette.grey[900],
            backgroundSize: "cover",
            backgroundPosition: "center",
          }}
        />
        <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
          <Box
            sx={{
              my: 0,
              mx: 4,
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              marginTop: 20,
              border: 0,
              height: 750,
            }}
          >
            <Avatar style={avatarStyle} sx={{ height: 85, width: 85, mt: 12 }}>
              <PersonOutlineOutlinedIcon sx={{ height: 50, width: 50 }} />
            </Avatar>

            <Box
              component="form"
              noValidate
              onSubmit={handleSubmit}
              sx={{ mt: 2, border: 0 }}
            >
              <Grid
                container
                direction="column"
                justifyContent="center"
                alignItems="center"
              >
                <Grid item>
                  <TextField
                    margin="normal"
                    required
                    name="email"
                    type="username"
                    label="Email address"
                    autoComplete="username"
                    variant="outlined"
                    autoFocus
                    sx={{ mb: 3, width: 500 }}
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <PersonOutlineOutlinedIcon />
                        </InputAdornment>
                      ),
                    }}
                  />
                </Grid>
                <Grid item>
                  <TextField
                    margin="normal"
                    required
                    name="password"
                    label="Password"
                    type={values.showPassword ? "text" : "password"}
                    onChange={handlePasswordChange("password")}
                    value={values.password}
                    id="password"
                    autoComplete="current-password"
                    sx={{ width: 500 }}
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <LockOutlinedIcon />
                        </InputAdornment>
                      ),
                      endAdornment: (
                        <InputAdornment position="end">
                          <IconButton
                            onClick={handleClickShowPassword}
                            onMouseDown={handleMouseDownPassword}
                          >
                            {values.showPassword ? (
                              <VisibilityIcon />
                            ) : (
                              <VisibilityOffIcon />
                            )}
                          </IconButton>
                        </InputAdornment>
                      ),
                    }}
                  />
                </Grid>
              </Grid>

              <Grid container>
                <Grid
                  item
                  xs
                  display="flex"
                  justifyContent="flex-end"
                  sx={{ mr: 13 }}
                >
                  <FormControlLabel
                    style={{ color: "#e05414" }}
                    control={
                      <Checkbox
                        icon={<CheckBoxOutlineBlankRoundedIcon />}
                        checkedIcon={<CheckBoxIcon />}
                      />
                    }
                    label="Remember me"
                  />
                </Grid>
                <Grid container justifyContent="center">
                  <Button
                    type="submit"
                    variant="contained"
                    color="primary"
                    sx={{ mt: 0, mb: 2, width: 150, height: 50 }}
                  >
                    Login
                  </Button>
                </Grid>
                <Grid container>
                  <Grid item xs>
                    <Typography align="center" sx={{ my: 1 }}>
                      ━━━━━━━━━━ OR ━━━━━━━━━━{" "}
                    </Typography>
                  </Grid>
                </Grid>

                <Box sx={{ border: 0, flexGrow: 1 }}>
                  <Stack justifyContent="center" direction="row" spacing={1}>
                    <Typography align="center" justifyContent="flex-end">
                      Don't have an account yet?
                    </Typography>
                    <Link
                      href="/Register"
                      fontSize="36"
                      style={{ color: "#e05414" }}
                    >
                      Register now
                    </Link>
                  </Stack>
                </Box>
              </Grid>
            </Box>
          </Box>
        </Grid>
      </Grid>
    </ThemeProvider>
  );
}
