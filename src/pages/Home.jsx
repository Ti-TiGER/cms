import React from "react";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import { motion } from "framer-motion";

//import component
import Appbar from "../components/appbar";
import Homecard from "../components/Database page/homecard";

export default function Home() {
  const mdTheme = createTheme({
    palette: {
      primary: {
        main: "#001345",
        info: "#e7e8fc",
      },
      secondary: {
        main: "#878787",
        info: "#ffffff",
      },
      neutral: {
        main: "#ffffff",
        contrastText: "#fff",
      },
    },
  });

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
    >
      <ThemeProvider theme={mdTheme}>
        <CssBaseline />
        <Grid container spacing={18}>
          <Grid item xs={12} display="flex">
            <Box
              sx={{
                border: 0,
                bgcolor: "primary.info",
                width: "auto",
                height: "auto",
                minHeight: 890,
                borderLeft: 71,
                borderColor: "primary.main",
              }}
            >
              <Appbar />
              <Grid container sx={{ pt: 5, mt: 5, mb: 4 }}>
                <Grid item xs={12}>
                  <Box display="flex" sx={{ pl: 86, border: 0 }}>
                    <Box
                      component="img"
                      sx={{
                        border: 0,
                        pt: 1,
                        ml: 10,
                        flexGrow: 1,
                        maxWidth: 350,
                        maxHeight: 350,
                      }}
                      alt="Store House Logo."
                      src="https://cdn.discordapp.com/attachments/885453987426492437/996344641345560577/unknown.png"
                    ></Box>
                  </Box>

                  <Box
                    justifyContent="center"
                    sx={{ display: "flex", pl: 7, flexGrow: 0 }}
                  >
                    <Homecard />
                  </Box>
                </Grid>
              </Grid>
            </Box>
          </Grid>
        </Grid>
      </ThemeProvider>
    </motion.div>
  );
}
