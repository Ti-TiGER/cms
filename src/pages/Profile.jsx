import React, { useEffect, useState } from "react";

// MUI import
import CssBaseline from "@mui/material/CssBaseline";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Avatar from "@mui/material/Avatar";
import Typography from "@mui/material/Typography";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import Card from "@mui/material/Card";
import Stack from "@mui/material/Stack";

//import component
import Appbar from "../components/appbar";

const theme = createTheme({
  palette: {
    primary: {
      main: "#142454",
    },
  },
});

export default function Profile() {
  const user = JSON.parse(localStorage.getItem("user"));
  const id = user.user_id;

  useEffect(() => {
    fetch("https://crud-wms.herokuapp.com/users/" + id)
      .then((res) => res.json())
      .then((result) => {
        setFname(result.fname);
        setLname(result.lname);
        setEmail(result.email);
        setAvatar(result.avatar);
        setContact(result.contact);
        setRole(result.role_name);
      });
  }, []);

  const [fname, setFname] = useState("");
  const [lname, setLname] = useState("");
  const [email, setEmail] = useState("");
  const [avatar, setAvatar] = useState("");
  const [contact, setContact] = useState("");
  const [role, setRole] = useState("");

  const UpdateUser = (user_id) => {
    window.location = "/UpdateProfile/" + user_id;
  };

  return (
    <ThemeProvider theme={theme}>
      <Appbar />
      <Grid container component="main" sx={{ height: "100vh" }}>
        <CssBaseline />
        <Grid
          item
          xs={false}
          sm={4}
          md={7}
          sx={{
            backgroundImage:
              "url(https://cdn.discordapp.com/attachments/744696824220221502/984030671427231775/unknown.png)",
            backgroundRepeat: "no-repeat",
            backgroundColor: (t) =>
              t.palette.mode === "light"
                ? t.palette.grey[50]
                : t.palette.grey[900],
            backgroundSize: "cover",
            backgroundPosition: "center",
          }}
        />
        <Grid
          item
          justifyContent="center"
          xs={12}
          sm={8}
          md={5}
          component={Paper}
          sx={{ pt: 22, backgroundColor: "#e7e8fc" }}
        >
          <Grid
            item
            sx={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Avatar
              alt="prof_pic"
              src={avatar}
              sx={{
                border: 15,
                borderColor: "primary.main",
                width: theme.spacing(40),
                height: theme.spacing(40),
                boxShadow: theme.shadows[3],
                mb: 3,
              }}
            />
            <Card raised="true" sx={{ width: 600, borderRadius: 5, p: 3 }}>
              <Stack direction="row">
                <Typography
                  sx={{
                    fontSize: 36,
                    mb: 2,
                    color: "primary.main",
                    flexGrow: 1,
                  }}
                >
                  My Profile
                </Typography>
                <Button
                  variant="contained"
                  onClick={() => UpdateUser(user.user_id)}
                  sx={{
                    width: 100,
                    height: 40,
                    borderRadius: 10,
                    mt: 0.75,
                    mr: 2,
                  }}
                >
                  EDIT
                </Button>
              </Stack>
              <Typography sx={{ fontSize: 22 }}>
                Name : {fname} {lname}
              </Typography>
              <Typography sx={{ fontSize: 22 }}>Email : {email} </Typography>
              <Typography sx={{ fontSize: 22 }}>
                Contact number : {contact}{" "}
              </Typography>
              <Typography sx={{ fontSize: 22 }}>Role : {role} </Typography>
            </Card>
          </Grid>
        </Grid>
      </Grid>
    </ThemeProvider>
  );
}
