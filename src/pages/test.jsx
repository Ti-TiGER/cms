import React, { useEffect, useState } from "react";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import { CardActionArea } from "@mui/material";
import { styled } from "@mui/material/styles";
import Collapse from "@mui/material/Collapse";
import IconButton from "@mui/material/IconButton";
import CardActions from "@mui/material/CardActions";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";

import { createTheme, ThemeProvider } from "@mui/material/styles";

import ErrorRoundedIcon from "@mui/icons-material/ErrorRounded";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

export default function UserCreate() {
  const theme = createTheme({
    palette: {
      primary: {
        main: "#142454",
      },
    },
  });

  const avatarStyle = { backgroundColor: "#041444" };

  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const [items, setItems] = useState([]);

  const [expanded, setExpanded] = useState(false);
  const [selectedId, setSelectedId] = useState(-1);
  const handleExpandClick = (orderId) => {
    if (selectedId === orderId) {
      setSelectedId(-1);
    } else {
      setSelectedId(orderId);
    }
  };

  return (
    <ThemeProvider theme={theme}>
      <Grid container component="main" sx={{ height: "100vh" }}>
        {items.map((order) => (
          <Grid key={order.id}>
            <Card className={classes.card}>
              <CardContent>//some content here</CardContent>
              <CardActions disableSpacing>
                <Tooltip title="Show More">
                  <IconButton
                    className={clsx(classes.expand, {
                      [classes.expandOpen]: expanded,
                    })}
                    onClick={() => handleExpandClick(order.id)}
                    aria-expanded={expanded}
                    aria-label="show more"
                    color="secondary"
                    style={{ margin: "0 auto" }}
                  >
                    <ExpandMoreIcon fontSize="large" />
                  </IconButton>
                </Tooltip>
              </CardActions>
              <Collapse
                in={order.id === selectedId ? true : false}
                timeout="auto"
                unmountOnExit
              >
                <CardContent>// content inside the expanded card</CardContent>
              </Collapse>
            </Card>
          </Grid>
        ))}
      </Grid>
    </ThemeProvider>
  );
}
