import React, { useEffect, useState } from "react";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import { CardActionArea } from "@mui/material";
import Button from "@mui/material/Button";
import { Link } from "react-router-dom";

import AddIcon from "@mui/icons-material/Add";
import DeleteForeverRoundedIcon from "@mui/icons-material/DeleteForeverRounded";

import Swal from "sweetalert2";

//import component
import Appbar from "../components/appbar";

export default function Categorylist() {
  const [category, setCategory] = useState([]);
  useEffect(() => {
    CategoryGet();
  }, []);

  const CategoryGet = () => {
    fetch("https://crud-wms.herokuapp.com/category")
      .then((res) => res.json())
      .then((rows) => {
        setCategory(rows);
      });
  };

  const getCatagorizedpd = (category_id) => {
    window.location = "/Category/" + category_id;
  };

  const CategoryUpdate = (category_id) => {
    window.location = "/UpdateCategory/" + category_id;
  };

  const CategoryDelete = (category_id) => {
    var data = {
      category_id: category_id,
    };
    fetch("https://crud-wms.herokuapp.com/deletecategory", {
      method: "DELETE",
      headers: {
        Accept: "application/form-data",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((res) => res.json())
      .then((rows) => {
        if (rows["status"] === "ok") {
          Swal.fire({
            icon: "success",
            title: "Deleted!",
            text: rows["message"],
            toast: true,
            position: "top-end",
            showConfirmButton: false,
            timer: 2500,
            timerProgressBar: true,
          });
          CategoryGet();
          window.location = "/Category";
        }
      });
  };

  const user = JSON.parse(localStorage.getItem("user"));

  const role = JSON.parse(localStorage.getItem("user"));

  const mdTheme = createTheme({
    palette: {
      primary: {
        main: "#001345",
        info: "#e7e8fc",
      },
      secondary: {
        main: "#ffffff",
        info: "#433100",
      },
    },
  });

  if (role.role_id === 1) {
    return (
      <ThemeProvider theme={mdTheme}>
        <CssBaseline />
        <Grid container>
          <Grid item xs={12} sx={{ display: "flex" }}>
            <Box
              sx={{
                bgcolor: "primary.info",
                width: "auto",
                height: "auto",
                minHeight: 890,
                borderLeft: 71,
                borderColor: "primary.main",
              }}
            >
              <Appbar />
              <Grid container sx={{ pt: 5, mt: 5, mb: 4 }}>
                <Grid item xs={12}>
                  <Box display="flex">
                    <Box sx={{ flexGrow: 1, mt: 5, pl: 36 }}>
                      <Typography
                        align="center"
                        sx={{
                          color: "primary.main",
                          justifyContent: "flex-start",
                          fontSize: "50px",
                          fontWeight: 500,
                          fontFamily: "",
                        }}
                      >
                        Category
                      </Typography>
                    </Box>
                    <Link to="/CategoryCreate">
                      <Button
                        variant="contained"
                        color="primary"
                        sx={{ width: 150, height: 50, mr: 12, mt: 8 }}
                      >
                        <AddIcon sx={{ mr: 1 }} />
                        CREATE
                      </Button>
                    </Link>
                  </Box>

                  <Grid container display="flex" sx={{ border: 0, ml: 6 }}>
                    {category.map((category) => (
                      <Grid
                        item
                        key={category.category_id}
                        alignItems="center"
                        display="flex"
                        xs={3}
                      >
                        <Card
                          raised="true"
                          align="center"
                          sx={{
                            width: 345,
                            p: 3,
                            m: 3,
                            backgroundColor: "#607EAA",
                            borderRadius: 5,
                          }}
                        >
                          <CardActionArea
                            onClick={() =>
                              getCatagorizedpd(category.category_id)
                            }
                          >
                            <CardMedia
                              component="img"
                              height="300"
                              image={category.image}
                            />
                            <CardContent>
                              <Typography
                                gutterBottom
                                variant="h5"
                                component="div"
                                sx={{ color: "#ffffff" }}
                              >
                                {category.category_name}
                              </Typography>
                            </CardContent>
                          </CardActionArea>
                          <Box display="flex" justifyContent="center">
                            <Button
                              variant="contained"
                              sx={{
                                borderRadius: 2,
                                width: 250,
                                backgroundColor: "#828aa1",
                                color: "#ffffff",
                              }}
                              onClick={() =>
                                CategoryUpdate(category.category_id)
                              }
                            >
                              <Typography sx={{ fontSize: 18 }}>
                                Edit
                              </Typography>
                            </Button>
                            <Button
                              color="error"
                              size="small"
                              onClick={() =>
                                CategoryDelete(category.category_id)
                              }
                            >
                              <DeleteForeverRoundedIcon fontSize="large" />
                            </Button>
                          </Box>
                        </Card>
                      </Grid>
                    ))}
                  </Grid>
                </Grid>
              </Grid>
            </Box>
          </Grid>
        </Grid>
      </ThemeProvider>
    );
  } else {
    return (
      <ThemeProvider theme={mdTheme}>
        <CssBaseline />
        <Grid container>
          <Grid item xs={12} sx={{ display: "flex" }}>
            <Box
              sx={{
                bgcolor: "primary.info",
                width: "auto",
                height: "auto",
                minHeight: 890,
                borderLeft: 71,
                borderColor: "primary.main",
              }}
            >
              <Appbar />
              <Grid container sx={{ pt: 5, mt: 5, mb: 4 }}>
                <Grid item xs={12}>
                  <Box display="flex">
                    <Box sx={{ flexGrow: 1, mt: 5, pl: 5 }}>
                      <Typography
                        align="center"
                        sx={{
                          color: "primary.main",
                          justifyContent: "flex-start",
                          fontSize: "50px",
                          fontWeight: 500,
                          fontFamily: "",
                        }}
                      >
                        Category
                      </Typography>
                    </Box>
                  </Box>

                  <Grid container display="flex" sx={{ border: 0, ml: 6 }}>
                    {category.map((category) => (
                      <Grid
                        item
                        key={category.category_id}
                        alignItems="center"
                        display="flex"
                        xs={3}
                      >
                        <Card
                          raised="true"
                          align="center"
                          sx={{
                            width: 345,
                            p: 3,
                            m: 3,
                            backgroundColor: "#607EAA",
                            borderRadius: 5,
                          }}
                        >
                          <CardActionArea
                            onClick={() =>
                              getCatagorizedpd(category.category_id)
                            }
                          >
                            <CardMedia
                              component="img"
                              height="300"
                              image={category.image}
                            />
                            <CardContent>
                              <Typography
                                gutterBottom
                                variant="h5"
                                component="div"
                                sx={{ color: "#ffffff" }}
                              >
                                {category.category_name}
                              </Typography>
                            </CardContent>
                          </CardActionArea>
                        </Card>
                      </Grid>
                    ))}
                  </Grid>
                </Grid>
              </Grid>
            </Box>
          </Grid>
        </Grid>
      </ThemeProvider>
    );
  }
}
