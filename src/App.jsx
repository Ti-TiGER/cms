import Users from "./pages/Users";
import UserUpdate from "./components/UserUpdate";
import UpdateProfile from "./components/UpdateProfile";

import Profile from "./pages/Profile";

import Register from "./pages/Register";
import Login from "./pages/Login";

import Home from "./pages/Home";
import Dashboard from "./pages/Dashboard";

import Product from "./pages/Products";
import ProductCreate from "./components/Database page/ProductCreate";
import ProductUpdate from "./components/Database page/ProductUpdate";

import Category from "./pages/Category";
import Categorizedpd from "./components/Database page/Catagorizedpd";
import CategoryCreate from "./components/Database page/CategoryCreate";
import CategoryUpdate from "./components/Database page/CategoryUpdate";

import Tag from "./pages/Tag";
import TagUpdate from "./components/Database page/TagUpdate";
import MultagsUpdate from "./components/Database page/MultagsUpdate";

import Missing from "./pages/Missing";

import Test from "./pages/test";

import { Routes, Route } from "react-router-dom";

function App() {
  return (
    <Routes>
      {/* public routes */}
      <Route path="/" element={<Login />} />
      <Route path="Register" element={<Register />} />

      <Route path="Users" element={<Users />} />
      <Route path="Profile" element={<Profile />} />
      <Route path="Update/:id" element={<UserUpdate />} />
      <Route path="Updateprofile/:id" element={<UpdateProfile />} />

      <Route path="Products" element={<Product />} />
      <Route path="ProductRegister" element={<ProductCreate />} />
      <Route path="Updatepd/:id" element={<ProductUpdate />} />

      <Route path="Home" element={<Home />} />
      <Route path="Dashboard" element={<Dashboard />} />

      <Route path="Category" element={<Category />} />
      <Route path="CategoryCreate" element={<CategoryCreate />} />
      <Route path="Category/:id" element={<Categorizedpd />} />
      <Route path="UpdateCategory/:id" element={<CategoryUpdate />} />

      <Route path="Tag" element={<Tag />} />
      <Route path="UpdateTag/:id" element={<TagUpdate />} />
      <Route path="UpdatemultipleTags/:id" element={<MultagsUpdate />} />

      {/* catch all */}
      <Route path="*" element={<Missing />} />
    </Routes>
  );
}

export default App;
