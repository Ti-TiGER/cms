import React, { useEffect, useState } from "react";

// MUI import
import { styled, createTheme, ThemeProvider } from "@mui/material/styles";

import Avatar from "@mui/material/Avatar";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import Link from "@mui/material/Link";
import Tooltip from "@mui/material/Tooltip";
import Menu from "@mui/material/Menu";
import MenuList from "@mui/material/MenuList";
import MenuItem from "@mui/material/MenuItem";

import Swal from "sweetalert2";

//Icon Stuff
import HomeIcon from "@mui/icons-material/Home";
import DashboardIcon from "@mui/icons-material/Dashboard";
import StorageIcon from "@mui/icons-material/Storage";
import ListAltIcon from "@mui/icons-material/ListAlt";
import FactCheckIcon from "@mui/icons-material/FactCheck";
import MenuIcon from "@mui/icons-material/Menu";
import Logout from "@mui/icons-material/Logout";
import box from "./icon/1524539.png";
import AutoAwesomeMosaicIcon from "@mui/icons-material/AutoAwesomeMosaic";

export default function appbar() {
  const mdTheme = createTheme({
    palette: {
      primary: {
        main: "#001345",
        info: "#e7e8fc",
      },
      secondary: {
        main: "#878787",
      },
    },

    typography: {
      fontFamily: [
        "-apple-system",
        "BlinkMacSystemFont",
        '"Segoe UI"',
        "Roboto",
        '"Helvetica Neue"',
        "Arial",
        "sans-serif",
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
      ].join(","),
    },
  });

  const user = JSON.parse(localStorage.getItem("user"));

  const id = user.user_id;

  useEffect(() => {
    fetch("https://crud-wms.herokuapp.com/users/" + id)
      .then((res) => res.json())
      .then((result) => {
        setFname(result.fname);
        setLname(result.lname);
        setAvatar(result.avatar);
      });
  }, []);

  const [fname, setFname] = useState("");
  const [lname, setLname] = useState("");
  const [avatar, setAvatar] = useState("");

  const roleID = JSON.parse(localStorage.getItem("user"));

  const handleLogout = (event) => {
    event.preventDefault();
    localStorage.removeItem("token");
    localStorage.removeItem("user");

    Swal.fire({
      icon: "success",
      title: "Logout!",
      text: "logged out successfully",
      // toast: true,
      // position: "top-end",
      showConfirmButton: false,
      timer: 1500,
      timerProgressBar: true,
    });

    window.setTimeout(function () {
      window.location.href = "/";
    }, 1500);
  };

  const [anchorElNav, setAnchorElNav] = React.useState(null);
  const [anchorElUser, setAnchorElUser] = React.useState(null);
  const [anchorEl, setAnchorEl] = React.useState(null);

  const open = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  if (roleID.role_id === 1) {
    return (
      <ThemeProvider theme={mdTheme}>
        <Container maxWidth="lg" sx={{ mb: 0 }}>
          <AppBar
            elevation={0}
            sx={{ bgcolor: "primary", border: 0 }}
            position="absolute"
          >
            <Toolbar>
              <Box
                sx={{
                  flexGrow: 1,
                  border: 1,
                  display: { xs: "flex", md: "none" },
                }}
              >
                <IconButton
                  size="large"
                  aria-label="account of current user"
                  aria-controls="menu-appbar"
                  aria-haspopup="true"
                  onClick={handleOpenNavMenu}
                  color="inherit"
                >
                  <MenuIcon />
                </IconButton>
                <Menu
                  id="menu-appbar"
                  anchorEl={anchorElNav}
                  anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "left",
                  }}
                  keepMounted
                  transformOrigin={{
                    vertical: "top",
                    horizontal: "left",
                  }}
                  open={Boolean(anchorElNav)}
                  onClose={handleCloseNavMenu}
                  sx={{
                    display: { xs: "block", md: "none" },
                  }}
                >
                  <MenuItem onClick={handleCloseNavMenu}>
                    <Button
                      variant="text"
                      sx={{
                        color: "primary",
                      }}
                      href="/Home"
                    >
                      <HomeIcon sx={{ mr: 1 }} />
                      Home
                    </Button>

                    <Button
                      variant="text"
                      sx={{
                        color: "primary",
                      }}
                      href="/Category"
                    >
                      <ListAltIcon sx={{ mr: 1 }} />
                      Category
                    </Button>

                    <Button
                      variant="text"
                      sx={{
                        color: "primary",
                      }}
                      href="/Products"
                    >
                      <img scr={box} />
                      <StorageIcon sx={{ mr: 1 }} />
                      Product list
                    </Button>

                    <Button
                      variant="text"
                      sx={{
                        color: "primary",
                      }}
                      href="/Tag"
                    >
                      <FactCheckIcon sx={{ mr: 1 }} />
                      Tag Checkup
                    </Button>
                  </MenuItem>
                </Menu>
              </Box>

              <Box
                sx={{
                  flexGrow: 1,
                  border: 0,
                  display: { xs: "none", md: "flex" },
                }}
              >
                <Button href="/Home">
                  <Link
                    component="img"
                    sx={{
                      border: 0,
                      flexGrow: 0,
                      maxWidth: 280,
                      maxHeight: 250,
                    }}
                    alt="Store House Logo."
                    src="https://cdn.discordapp.com/attachments/885453987426492437/996348191161188392/unknown.png"
                  ></Link>
                </Button>
                <Button
                  variant="text"
                  sx={{
                    color: "white",
                    fontFamily: "",
                    fontSize: "24px",
                    fontWeight: 400,
                    border: 0,
                    pr: 5,
                  }}
                  href="/Home"
                >
                  <HomeIcon sx={{ mr: 1 }} />
                  Home
                </Button>

                <Button
                  variant="text"
                  sx={{
                    color: "white",
                    fontFamily: "",
                    fontSize: "24px",
                    fontWeight: 400,
                    pr: 5,
                  }}
                  href="/Dashboard"
                >
                  <DashboardIcon sx={{ mr: 1 }} />
                  Dashboard
                </Button>

                <Button
                  variant="text"
                  sx={{
                    color: "white",
                    fontFamily: "",
                    fontSize: "24px",
                    fontWeight: 400,
                    pr: 5,
                  }}
                  href="/Category"
                >
                  <AutoAwesomeMosaicIcon sx={{ mr: 1 }} />
                  Category
                </Button>

                <Button
                  variant="text"
                  sx={{
                    color: "white",
                    fontFamily: "",
                    fontSize: "24px",
                    fontWeight: 400,
                    pr: 5,
                  }}
                  href="/Products"
                >
                  <img scr={box} />
                  <StorageIcon sx={{ mr: 1 }} />
                  Product list
                </Button>

                <Button
                  variant="text"
                  sx={{
                    color: "white",
                    fontFamily: "",
                    fontSize: "24px",
                    fontWeight: 400,
                  }}
                  href="/Tag"
                >
                  <FactCheckIcon sx={{ mr: 1 }} />
                  Tag Checkup
                </Button>
              </Box>

              <Box sx={{ flexGrow: 0 }}>
                <Tooltip title="Open settings">
                  <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                    <Avatar alt="Profile avatar" src={avatar} />
                    <Typography
                      sx={{
                        ml: 2,
                        color: "white",
                        display: "block",
                        fontSize: "24px",
                      }}
                    >
                      {fname} {lname}
                    </Typography>
                  </IconButton>
                </Tooltip>
                <Menu
                  sx={{ mt: "45px" }}
                  id="menu-appbar"
                  anchorEl={anchorElUser}
                  anchorOrigin={{
                    vertical: "top",
                    horizontal: "right",
                  }}
                  keepMounted
                  transformOrigin={{
                    vertical: "top",
                    horizontal: "right",
                  }}
                  open={Boolean(anchorElUser)}
                  onClose={handleCloseUserMenu}
                >
                  <MenuList onClick={handleCloseUserMenu}>
                    <MenuItem>
                      <Button
                        sx={{ mx: 3 }}
                        href="/Profile"
                        variant="contained"
                      >
                        Profile
                      </Button>
                    </MenuItem>

                    <MenuItem>
                      <Button sx={{ mx: 3 }} href="/Users" variant="contained">
                        Manage Users
                      </Button>
                    </MenuItem>

                    <MenuItem>
                      <Button
                        sx={{ mx: 3 }}
                        href="/Dashboard"
                        variant="contained"
                      >
                        Dashboard
                      </Button>
                    </MenuItem>

                    <MenuItem>
                      <Button
                        sx={{ mx: 3 }}
                        variant="contained"
                        onClick={handleLogout}
                      >
                        Logout
                        <Logout sx={{ pl: 0.5 }} fontSize="small" />
                      </Button>
                    </MenuItem>
                  </MenuList>
                </Menu>
              </Box>
            </Toolbar>
          </AppBar>
        </Container>
      </ThemeProvider>
    );
  } else {
    return (
      <ThemeProvider theme={mdTheme}>
        <Container maxWidth="lg" sx={{ mb: 0 }}>
          <AppBar
            elevation={0}
            sx={{ bgcolor: "primary", border: 0 }}
            position="absolute"
          >
            <Toolbar>
              <Box
                sx={{
                  flexGrow: 1,
                  border: 1,
                  display: { xs: "flex", md: "none" },
                }}
              >
                <IconButton
                  size="large"
                  aria-label="account of current user"
                  aria-controls="menu-appbar"
                  aria-haspopup="true"
                  onClick={handleOpenNavMenu}
                  color="inherit"
                >
                  <MenuIcon />
                </IconButton>
                <Menu
                  id="menu-appbar"
                  anchorEl={anchorElNav}
                  anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "left",
                  }}
                  keepMounted
                  transformOrigin={{
                    vertical: "top",
                    horizontal: "left",
                  }}
                  open={Boolean(anchorElNav)}
                  onClose={handleCloseNavMenu}
                  sx={{
                    display: { xs: "block", md: "none" },
                  }}
                >
                  <MenuItem onClick={handleCloseNavMenu}>
                    <Button
                      variant="text"
                      sx={{
                        color: "primary",
                      }}
                      href="/Home"
                    >
                      <HomeIcon sx={{ mr: 1 }} />
                      Home
                    </Button>

                    <Button
                      variant="text"
                      sx={{
                        color: "primary",
                        fontFamily: "",
                        fontSize: "24px",
                        fontWeight: 400,
                        pr: 5,
                      }}
                      href="/Category"
                    >
                      <AutoAwesomeMosaicIcon sx={{ mr: 1 }} />
                      Category
                    </Button>
                    <Button
                      variant="text"
                      sx={{
                        color: "primary",
                        fontFamily: "",
                        fontSize: "24px",
                        fontWeight: 400,
                        pr: 5,
                      }}
                      href="/Products"
                    >
                      <img scr={box} />
                      <StorageIcon sx={{ mr: 1 }} />
                      Product list
                    </Button>
                  </MenuItem>
                </Menu>
              </Box>

              <Box
                sx={{
                  flexGrow: 1,
                  border: 0,
                  display: { xs: "none", md: "flex" },
                }}
              >
                <Button href="/Home">
                  <Link
                    component="img"
                    sx={{
                      border: 0,
                      flexGrow: 0,
                      maxWidth: 280,
                      maxHeight: 250,
                    }}
                    alt="Store House Logo."
                    src="https://cdn.discordapp.com/attachments/885453987426492437/996348191161188392/unknown.png"
                  ></Link>
                </Button>
                <Button
                  variant="text"
                  sx={{
                    color: "white",
                    fontFamily: "",
                    fontSize: "24px",
                    fontWeight: 400,
                    border: 0,
                    pr: 5,
                  }}
                  href="/Home"
                >
                  <HomeIcon sx={{ mr: 1 }} />
                  Home
                </Button>

                <Button
                  variant="text"
                  sx={{
                    color: "white",
                    fontFamily: "",
                    fontSize: "24px",
                    fontWeight: 400,
                    pr: 5,
                  }}
                  href="/Category"
                >
                  <AutoAwesomeMosaicIcon sx={{ mr: 1 }} />
                  Category
                </Button>

                <Button
                  variant="text"
                  sx={{
                    color: "white",
                    fontFamily: "",
                    fontSize: "24px",
                    fontWeight: 400,
                    pr: 5,
                  }}
                  href="/Products"
                >
                  <img scr={box} />
                  <StorageIcon sx={{ mr: 1 }} />
                  Product list
                </Button>
              </Box>

              <Box sx={{ flexGrow: 0 }}>
                <Tooltip title="Open settings">
                  <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                    <Avatar alt="Profile avatar" src={user.avatar} />
                    <Typography
                      sx={{
                        ml: 2,
                        color: "white",
                        display: "block",
                        fontSize: "24px",
                      }}
                    >
                      {user.fname} {user.lname}
                    </Typography>
                  </IconButton>
                </Tooltip>
                <Menu
                  sx={{ mt: "45px" }}
                  id="menu-appbar"
                  anchorEl={anchorElUser}
                  anchorOrigin={{
                    vertical: "top",
                    horizontal: "right",
                  }}
                  keepMounted
                  transformOrigin={{
                    vertical: "top",
                    horizontal: "right",
                  }}
                  open={Boolean(anchorElUser)}
                  onClose={handleCloseUserMenu}
                >
                  <MenuList onClick={handleCloseUserMenu}>
                    <MenuItem>
                      <Button
                        sx={{ mx: 3 }}
                        href="/Profile"
                        variant="contained"
                      >
                        Profile
                      </Button>
                    </MenuItem>

                    <MenuItem>
                      <Button
                        sx={{ mx: 3 }}
                        variant="contained"
                        onClick={handleLogout}
                      >
                        Logout
                        <Logout sx={{ pl: 0.5 }} fontSize="small" />
                      </Button>
                    </MenuItem>
                  </MenuList>
                </Menu>
              </Box>
            </Toolbar>
          </AppBar>
        </Container>
      </ThemeProvider>
    );
  }
}
