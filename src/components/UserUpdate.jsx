import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import CssBaseline from "@mui/material/CssBaseline";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Avatar from "@mui/material/Avatar";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import Select from "@mui/material/Select";

import { createTheme, ThemeProvider } from "@mui/material/styles";

import Swal from "sweetalert2";

import ManageAccountsIcon from "@mui/icons-material/ManageAccounts";

export default function UserUpdate() {
  const { id } = useParams();
  useEffect(() => {
    fetch("https://crud-wms.herokuapp.com/users/" + id)
      .then((res) => res.json())
      .then((result) => {
        setFname(result.fname);
        setLname(result.lname);
        setEmail(result.email);
        setPassword(result.password);
        setAvatar(result.avatar);
        setContact(result.contact);
        setRole_id(result.role_id);
      });
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault();
    var data = {
      user_id: id,
      fname: fname,
      lname: lname,
      email: email,
      password: password,
      avatar: avatar,
      contact: contact,
      role_id: role_id,
    };
    fetch("https://crud-wms.herokuapp.com/update", {
      method: "PUT",
      headers: {
        Accept: "application/form-data",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((result) => {
        const ok = JSON.stringify(result.status);
        if (ok == ok) {
          Swal.fire({
            icon: "success",
            title: "Updated!",
            text: result["message"],
            timer: 2000,
            timerProgressBar: true,
            showConfirmButton: false,
          });
          window.setTimeout(function () {
            window.location.href = "/Users";
          }, 2000);
        }
      })

      .catch((error) => console.log("error", error));
  };

  const [fname, setFname] = useState("");
  const [lname, setLname] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [avatar, setAvatar] = useState("");
  const [contact, setContact] = useState("");
  const [role_id, setRole_id] = useState([]);
  const [role, setRole] = useState([]);
  useEffect(() => {
    RoleGet();
  }, []);

  const RoleGet = () => {
    fetch("https://crud-wms.herokuapp.com/roles")
      .then((res) => res.json())
      .then((rows) => {
        console.log(rows);
        setRole(rows);
      });
  };

  const theme = createTheme({
    palette: {
      primary: {
        main: "#142454",
      },
    },
  });

  const avatarStyle = { backgroundColor: "#041444" };

  return (
    <ThemeProvider theme={theme}>
      <Grid container component="main" sx={{ height: "100vh" }}>
        <CssBaseline />
        <Grid
          item
          xs={false}
          sm={4}
          md={7}
          sx={{
            backgroundImage:
              "url(https://cdn.discordapp.com/attachments/744696824220221502/984030671427231775/unknown.png)",
            backgroundRepeat: "no-repeat",
            backgroundColor: (t) =>
              t.palette.mode === "light"
                ? t.palette.grey[50]
                : t.palette.grey[900],
            backgroundSize: "cover",
            backgroundPosition: "center",
          }}
        />
        <Grid
          item
          align="center"
          xs={12}
          sm={8}
          md={5}
          component={Paper}
          elevation={6}
          square
        >
          <Box
            sx={{
              my: 0,
              mx: 4,
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              marginTop: 10,
              border: 0,
              height: 750,
            }}
          >
            <Avatar style={avatarStyle} sx={{ height: 85, width: 85 }}>
              <ManageAccountsIcon sx={{ height: 50, width: 50 }} />
            </Avatar>
            <Typography component="h1" variant="h5" sx={{ mt: 3, mb: 2 }}>
              Update User
            </Typography>

            <form onSubmit={handleSubmit}>
              <Grid
                display="flex"
                container
                sx={{ mt: 1, border: 0 }}
                spacing={2}
              >
                <Grid
                  container
                  justifyContent="flex-end"
                  alignItems="center"
                  xs={12}
                  sm={6}
                  sx={{ border: 0, mb: 1 }}
                >
                  <TextField
                    justifyContent="flex-end"
                    autoComplete="fname"
                    name="firstName"
                    variant="outlined"
                    required
                    id="firstName"
                    label="First Name"
                    value={fname}
                    sx={{ width: 237 }}
                    onChange={(e) => setFname(e.target.value)}
                    autoFocus
                  />
                </Grid>
                <Grid
                  container
                  justifyContent="flex-start"
                  xs={12}
                  sm={6}
                  sx={{ pl: 1 }}
                >
                  <TextField
                    variant="outlined"
                    required
                    id="lastName"
                    label="Last Name"
                    value={lname}
                    sx={{ width: 243 }}
                    onChange={(e) => setLname(e.target.value)}
                  />
                </Grid>
                <Grid item xs={12} sx={{ mb: 1, border: 0 }}>
                  <TextField
                    variant="outlined"
                    required
                    id="email"
                    label="Email"
                    value={email}
                    sx={{ width: 490 }}
                    onChange={(e) => setEmail(e.target.value)}
                  />
                </Grid>
                <Grid item xs={12} sx={{ mb: 1 }}>
                  <TextField
                    variant="outlined"
                    required
                    id="password"
                    label="Password"
                    value={password}
                    sx={{ width: 490 }}
                    onChange={(e) => setPassword(e.target.value)}
                  />
                </Grid>
                <Grid item xs={12} sx={{ mb: 1 }}>
                  <TextField
                    variant="outlined"
                    required
                    id="avatar"
                    label="User avatar"
                    value={avatar}
                    sx={{ width: 490 }}
                    onChange={(e) => setAvatar(e.target.value)}
                  />
                </Grid>
                <Grid item xs={12} sx={{ mb: 1 }}>
                  <TextField
                    variant="outlined"
                    required
                    id="contact"
                    label="Contact number"
                    value={contact}
                    sx={{ width: 490 }}
                    onChange={(e) => setContact(e.target.value)}
                  />
                </Grid>

                <Grid
                  item
                  justifyContent="center"
                  xs={12}
                  sx={{ mb: 1, pl: 2 }}
                >
                  <FormControl style={{ width: 490 }}>
                    <InputLabel id="demo-simple-select-label">
                      Please select a role
                    </InputLabel>

                    <Select
                      variant="outlined"
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      label="Please select a roles"
                      onChange={(e) => setRole_id(e.target.value)}
                    >
                      <MenuItem value={null}>RESET</MenuItem>
                      {role.map((role) => (
                        <MenuItem key={role.role_id} value={role.role_id}>
                          {role.role_name}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                </Grid>

                <Grid
                  item
                  xs={12}
                  sx={{ display: "flex", justifyContent: "center" }}
                >
                  <Button
                    type="submit"
                    variant="contained"
                    sx={{ width: 150, height: 50 }}
                  >
                    Submit Update
                  </Button>
                </Grid>
              </Grid>
            </form>
          </Box>
        </Grid>
      </Grid>
    </ThemeProvider>
  );
}
