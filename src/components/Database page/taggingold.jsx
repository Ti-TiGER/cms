import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import Swal from "sweetalert2";

// MUI import
import {
  styled,
  alpha,
  createTheme,
  useTheme,
  ThemeProvider,
} from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Link from "@mui/material/Link";
import Tooltip from "@mui/material/Tooltip";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import TextField from "@mui/material/TextField";
import Stack from "@mui/material/Stack";
import Autocomplete from "@mui/material/Autocomplete";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableFooter from "@mui/material/TableFooter";
import TablePagination from "@mui/material/TablePagination";

//Icon Stuff
import ClearIcon from "@mui/icons-material/Clear";
import SearchIcon from "@mui/icons-material/Search";
import FirstPageIcon from "@mui/icons-material/FirstPage";
import KeyboardArrowLeft from "@mui/icons-material/KeyboardArrowLeft";
import KeyboardArrowRight from "@mui/icons-material/KeyboardArrowRight";
import LastPageIcon from "@mui/icons-material/LastPage";
import MenuIcon from "@mui/icons-material/Menu";
import AdbIcon from "@mui/icons-material/Adb";
import LogoutIcon from "@mui/icons-material/Logout";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ShowChartOutlinedIcon from "@mui/icons-material/ShowChartOutlined";
import NotificationsNoneOutlinedIcon from "@mui/icons-material/NotificationsNoneOutlined";

//import component
import Appbar from "../appbar";

function Copyright(props) {
  return (
    <Typography
      variant="body2"
      color="text.secondary"
      align="center"
      {...props}
    >
      {"Copyright © "}
      <Link color="inherit" href="https://mui.com/">
        Dodeep.co.th&nbsp;
      </Link>
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

function TablePaginationActions(props) {
  const theme = useTheme();
  const { count, page, rowsPerPage, onPageChange } = props;

  const handleFirstPageButtonClick = (event) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <Box sx={{ flexShrink: 0, ml: 2.5 }}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowRight />
        ) : (
          <KeyboardArrowLeft />
        )}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowLeft />
        ) : (
          <KeyboardArrowRight />
        )}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </Box>
  );
}

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};

const mdTheme = createTheme({
  palette: {
    primary: {
      main: "#001345",
      info: "#e7e8fc",
    },
    secondary: {
      main: "#878787",
    },
  },
});

export default function Tagging() {
  useEffect(() => {
    const token = localStorage.getItem("token");
    fetch("https://tiger-crud.herokuapp.com/auth", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data["status"] === "Verified") {
          // Swal.fire({
          //   icon: "success",
          //   title: "Authentication Successful!",
          //   text: "Your authentication has been verified",
          //   toast: true,
          //   position: "bottom-end",
          //   timer: 2500,
          //   timerProgressBar: true,
          //   showConfirmButton: false,
          // });
          // alert("Your authentication has been verified");
        } else {
          Swal.fire({
            icon: "error",
            title: "Authentication Deny!",
            text: "Your authentication has been declined",
            showConfirmButton: false,
          });
          window.setTimeout(function () {
            window.location.href = "/";
          }, 500);
        }
      })
      .catch((error) => console.log("error", error));
  }, []);

  const [platform, setPlatform] = useState([]);
  const [searchText, setSearchText] = useState("");
  const [rows, setRows] = useState([]);
  useEffect(() => {
    let platformList = [
      {
        id: 1,
        status: "Activated",
        tagid: "1T3000E20020194716002013408C7301",
        date: "7/1/2022",
        time: "11.59 pm",
      },
      {
        id: 2,
        status: "Activated",
        tagid: "1T300031313135313731353632313808",
        date: "7/1/2022",
        time: "11.59 pm",
      },
      {
        id: 3,
        status: "Activated",
        tagid: "1T3000E20020194716020913908B0808",
        date: "7/1/2022",
        time: "11.59 pm",
      },
      {
        id: 4,
        status: "Pending",
        tagid: "1T3000FE4ED90063040046126099BF0E",
        date: "7/1/2022",
        time: "11.59 pm",
      },
      {
        id: 5,
        status: "Activated",
        tagid: "$2b$10$XZ7Z68hJJZiCGTkt6iMnkuc.um.kK1dXFibothBt9BWb2cIWmZWVe",
        date: null,
        time: "11.59 pm",
      },
      {
        id: 6,
        status: "Activated",
        tagid: "$2b$10$XZ7Z68hJJZiCGTkt6iMnkuc.um.kK1dXFibothBt9BWb2cIWmZWVe",
        date: "7/1/2022",
        time: "11.59 pm",
      },
      {
        id: 7,
        status: "Pending",
        tagid: "$2b$10$XZ7Z68hJJZiCGTkt6iMnkuc.um.kK1dXFibothBt9BWb2cIWmZWVe",
        date: "7/1/2022",
        time: "11.59 pm",
      },
      {
        id: 8,
        status: "Pending",
        tagid: "$2b$10$XZ7Z68hJJZiCGTkt6iMnkuc.um.kK1dXFibothBt9BWb2cIWmZWVe",
        date: "7/1/2022",
        time: "11.59 pm",
      },
      {
        id: 9,
        status: "Pending",
        tagid: "$2b$10$XZ7Z68hJJZiCGTkt6iMnkuc.um.kK1dXFibothBt9BWb2cIWmZWVe",
        date: "7/1/2022",
        time: "11.59 pm",
      },
      {
        id: 10,
        status: "Pending",
        tagid: "$2b$10$XZ7Z68hJJZiCGTkt6iMnkuc.um.kK1dXFibothBt9BWb2cIWmZWVe",
        date: "7/1/2022",
        time: "11.59 pm",
      },
      {
        id: 11,
        status: "Pending",
        tagid: "$2b$10$XZ7Z68hJJZiCGTkt6iMnkuc.um.kK1dXFibothBt9BWb2cIWmZWVe",
        date: "7/1/2022",
        time: "11.59 pm",
      },
      {
        id: 12,
        status: "Pending",
        tagid: "$2b$10$XZ7Z68hJJZiCGTkt6iMnkuc.um.kK1dXFibothBt9BWb2cIWmZWVe",
        date: "7/1/2022",
        time: "11.59 pm",
      },
      {
        id: 13,
        status: "Pending",
        tagid: "$2b$10$XZ7Z68hJJZiCGTkt6iMnkuc.um.kK1dXFibothBt9BWb2cIWmZWVe",
        date: "7/1/2022",
        time: "11.59 pm",
      },
      {
        id: 14,
        status: "Pending",
        tagid: "$2b$10$XZ7Z68hJJZiCGTkt6iMnkuc.um.kK1dXFibothBt9BWb2cIWmZWVe",
        date: "7/1/2022",
        time: "11.59 pm",
      },
      {
        id: 15,
        status: "Pending",
        tagid: "$2b$10$XZ7Z68hJJZiCGTkt6iMnkuc.um.kK1dXFibothBt9BWb2cIWmZWVe",
        date: "7/1/2022",
        time: "11.59 pm",
      },

      {
        id: 16,
        status: "Pending",
        tagid: "$2b$10$XZ7Z68hJJZiCGTkt6iMnkuc.um.kK1dXFibothBt9BWb2cIWmZWVe",
        date: "7/1/2022",
        time: "11.59 pm",
      },
      {
        id: 17,
        status: "Pending",
        tagid: "$2b$10$XZ7Z68hJJZiCGTkt6iMnkuc.um.kK1dXFibothBt9BWb2cIWmZWVe",
        date: "7/1/2022",
        time: "11.59 pm",
      },
      {
        id: 18,
        status: "Pending",
        tagid: "$2b$10$XZ7Z68hJJZiCGTkt6iMnkuc.um.kK1dXFibothBt9BWb2cIWmZWVe",
        date: "7/1/2022",
        time: "11.59 pm",
      },

      {
        id: 19,
        status: "Pending",
        tagid: "$2b$10$XZ7Z68hJJZiCGTkt6iMnkuc.um.kK1dXFibothBt9BWb2cIWmZWVe",
        date: "7/1/2022",
        time: "11.59 pm",
      },
      {
        id: 20,
        status: "Pending",
        tagid: "$2b$10$XZ7Z68hJJZiCGTkt6iMnkuc.um.kK1dXFibothBt9BWb2cIWmZWVe",
        date: "7/1/2022",
        time: "11.59 pm",
      },
      {
        id: 21,
        status: "Pending",
        tagid: "$2b$10$XZ7Z68hJJZiCGTkt6iMnkuc.um.kK1dXFibothBt9BWb2cIWmZWVe",
        date: "7/1/2022",
        time: "11.59 pm",
      },
      {
        id: 22,
        status: "Pending",
        tagid: "$2b$10$XZ7Z68hJJZiCGTkt6iMnkuc.um.kK1dXFibothBt9BWb2cIWmZWVe",
        date: "7/1/2022",
        time: "11.59 pm",
      },

      {
        id: 23,
        status: "Pending",
        tagid: "$2b$10$XZ7Z68hJJZiCGTkt6iMnkuc.um.kK1dXFibothBt9BWb2cIWmZWVe",
        date: "7/1/2022",
        time: "11.59 pm",
      },
      {
        id: 24,
        status: "Pending",
        tagid: "$2b$10$XZ7Z68hJJZiCGTkt6iMnkuc.um.kK1dXFibothBt9BWb2cIWmZWVe",
        date: "7/1/2022",
        time: "11.59 pm",
      },
      {
        id: 25,
        status: "Pending",
        tagid: "$2b$10$XZ7Z68hJJZiCGTkt6iMnkuc.um.kK1dXFibothBt9BWb2cIWmZWVe",
        date: "7/1/2022",
        time: "11.59 pm",
      },

      {
        id: 26,
        status: "Pending",
        tagid: "$2b$10$XZ7Z68hJJZiCGTkt6iMnkuc.um.kK1dXFibothBt9BWb2cIWmZWVe",
        date: "7/1/2022",
        time: "11.59 pm",
      },
      {
        id: 27,
        status: "Pending",
        tagid: "$2b$10$XZ7Z68hJJZiCGTkt6iMnkuc.um.kK1dXFibothBt9BWb2cIWmZWVe",
        date: "7/1/2022",
        time: "11.59 pm",
      },
      {
        id: 28,
        status: "Pending",
        tagid: "$2b$10$XZ7Z68hJJZiCGTkt6iMnkuc.um.kK1dXFibothBt9BWb2cIWmZWVe",
        date: "7/1/2022",
        time: "11.59 pm",
      },
    ];
    setPlatform(platformList);
    setRows(platformList);
  }, []);

  function escapeRegExp(value) {
    return value.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
  }

  const requestSearch = (searchValue) => {
    const searchRegex = new RegExp(escapeRegExp(searchValue), "i");
    const filteredRows = platform.filter((row) => {
      return Object.keys(row).some((field) => {
        return searchRegex.test(row[field]);
      });
    });
    setRows(filteredRows);
  };

  const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      fontSize: 20,
      backgroundColor: mdTheme.palette.primary.main,
      color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 16,
      color: mdTheme.palette.primary.main,
    },
  }));

  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    "&:last-child td, &:last-child th": {
      border: 0,
    },
  }));

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  return (
    <ThemeProvider theme={mdTheme}>
      <CssBaseline />
      <Grid container spacing={18}>
        <Grid item xs={12}>
          <Box
            sx={{
              bgcolor: "primary.info",
              width: "auto",
              height: "auto",
              minHeight: 900,
              borderLeft: 71,
              borderColor: "primary.main",
            }}
          >
            <Appbar />
            <Grid container sx={{ pt: 5, mt: 5, mb: 4 }}>
              <Grid item xs={12} sx={{ height: "auto", width: "auto" }}>
                <Box display="flex">
                  <Box sx={{ pl: 7, flexGrow: 1 }}>
                    <Typography
                      sx={{
                        color: "primary",
                        justifyContent: "flex-start",
                        fontSize: "32px",
                        fontWeight: 500,
                        fontFamily: "",
                      }}
                    >
                      Tag Status
                    </Typography>
                  </Box>
                  <Box
                    display="flex"
                    sx={{ justifyContent: "flex-end", mr: 15 }}
                  >
                    <TextField
                      variant="standard"
                      value={searchText}
                      onChange={(e) => {
                        setSearchText(e.target.value);
                        requestSearch(e.target.value);
                      }}
                      placeholder="Search..."
                      InputProps={{
                        startAdornment: (
                          <SearchIcon fontSize="medium" color="action" />
                        ),
                        endAdornment: (
                          <IconButton
                            title="Clear"
                            aria-label="Clear"
                            size="small"
                            style={{
                              visibility: searchText ? "visible" : "hidden",
                              borderRadius: "57%",
                              paddingRight: "1px",
                              margin: "0",
                              fontSize: "1.25rem",
                            }}
                            onClick={(e) => {
                              setSearchText("");
                              setRows(platform);
                            }}
                          >
                            <ClearIcon fontSize="medium" color="action" />
                          </IconButton>
                        ),
                      }}
                      sx={{
                        width: { xs: 1, sm: "auto" },
                        m: (theme) => theme.spacing(1, 0.5, 1.5),
                        "& .MuiSvgIcon-root": {
                          mr: 0.5,
                        },
                        "& .MuiInput-underline:before": {
                          borderBottom: 1,
                          borderColor: "divider",
                        },
                      }}
                    />
                  </Box>
                </Box>
                <Box
                  display="flex"
                  sx={{
                    justifyContent: "center",
                  }}
                >
                  <TableContainer component={Paper} sx={{ mx: 15 }}>
                    <Table sx={{ minWidth: 650 }} aria-label="simple table">
                      <TableHead>
                        <StyledTableRow>
                          <StyledTableCell align="center">ID</StyledTableCell>
                          <StyledTableCell align="left">
                            Tag&nbsp;id
                          </StyledTableCell>
                          <StyledTableCell align="center">
                            Status
                          </StyledTableCell>
                          <StyledTableCell align="center">Date</StyledTableCell>
                          <StyledTableCell align="center">Time</StyledTableCell>
                        </StyledTableRow>
                      </TableHead>
                      <TableBody>
                        {(rowsPerPage > 0
                          ? rows.slice(
                              page * rowsPerPage,
                              page * rowsPerPage + rowsPerPage
                            )
                          : rows
                        ).map((row) => (
                          <StyledTableRow key={row.name}>
                            <StyledTableCell
                              component="th"
                              scope="row"
                              align="center"
                            >
                              {row.id}
                            </StyledTableCell>
                            <StyledTableCell align="left">
                              {row.tagid}
                            </StyledTableCell>
                            <StyledTableCell align="center">
                              {row.status}
                            </StyledTableCell>
                            <StyledTableCell align="center">
                              {row.date}
                            </StyledTableCell>
                            <StyledTableCell align="center">
                              {row.time}
                            </StyledTableCell>
                          </StyledTableRow>
                        ))}
                      </TableBody>
                      <TableFooter>
                        <TableRow sx={{ borderTop: 0 }}>
                          <TablePagination
                            rowsPerPageOptions={[
                              10,
                              25,
                              50,
                              100,
                              { label: "All", value: -1 },
                            ]}
                            colSpan={2}
                            count={rows.length}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            SelectProps={{
                              inputProps: {
                                "aria-label": "rows per page",
                              },
                              native: true,
                            }}
                            onPageChange={handleChangePage}
                            onRowsPerPageChange={handleChangeRowsPerPage}
                            ActionsComponent={TablePaginationActions}
                          />
                        </TableRow>
                      </TableFooter>
                    </Table>
                  </TableContainer>
                </Box>
              </Grid>
            </Grid>

            <Copyright sx={{ pt: 4 }} />
          </Box>
        </Grid>
      </Grid>
    </ThemeProvider>
  );
}
