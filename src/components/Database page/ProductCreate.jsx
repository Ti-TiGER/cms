import React, { useState } from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Avatar from "@mui/material/Avatar";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";

import { createTheme, ThemeProvider } from "@mui/material/styles";

import Swal from "sweetalert2";

import ShoppingCartCheckoutIcon from "@mui/icons-material/ShoppingCartCheckout";

export default function ProductCreate() {
  const handleSubmit = (event) => {
    event.preventDefault();

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var data = {
      product_name: product_name,
      description: description,
      product_picture: product_picture,
      Quantity: Quantity,
    };
    fetch("https://crud-wms.herokuapp.com/createpd", {
      method: "POST",
      headers: {
        Accept: "application/form-data",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((res) => res.json())
      .then((result) => {
        if (result["status"] === "ok") {
          Swal.fire({
            icon: "success",
            title: "Created!",
            text: result["message"],
            showConfirmButton: false,
          });
          window.location.href = "/Products";
        } else if (result["status"] === "registered") {
          Swal.fire({
            icon: "error",
            title: "Email registered",
            text: result["message"],
            showConfirmButton: false,
            position: "top-end",
            toast: true,
            timer: 2500,
            timerProgressBar: true,
          });
        } else if (result["status"] === "error") {
          Swal.fire({
            icon: "error",
            title: "Create fail",
            text: result["message"],
            showConfirmButton: false,
          });
        }
      });
  };

  const [product_name, setProduct_name] = useState("");
  const [description, setDescription] = useState("");
  const [product_picture, setProduct_picture] = useState("");
  const [Quantity, setQuantity] = useState("");

  const theme = createTheme({
    palette: {
      primary: {
        main: "#142454",
      },
    },
  });

  const avatarStyle = { backgroundColor: "#041444" };

  return (
    <ThemeProvider theme={theme}>
      <Grid container component="main" sx={{ height: "100vh" }}>
        <CssBaseline />
        <Grid
          item
          xs={false}
          sm={4}
          md={7}
          sx={{
            backgroundImage:
              "url(https://cdn.discordapp.com/attachments/744696824220221502/984030671427231775/unknown.png)",
            backgroundRepeat: "no-repeat",
            backgroundColor: (t) =>
              t.palette.mode === "light"
                ? t.palette.grey[50]
                : t.palette.grey[900],
            backgroundSize: "cover",
            backgroundPosition: "center",
          }}
        />
        <Grid
          item
          align="center"
          xs={12}
          sm={8}
          md={5}
          component={Paper}
          elevation={6}
          square
        >
          <Box
            sx={{
              my: 0,
              mx: 4,
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              marginTop: 10,
              border: 0,
              height: 750,
            }}
          >
            <Grid align="center">
              <Avatar style={avatarStyle} sx={{ height: 85, width: 85 }}>
                <ShoppingCartCheckoutIcon sx={{ height: 50, width: 50 }} />
              </Avatar>
              <Typography component="h1" variant="h5" sx={{ mt: 3, mb: 2 }}>
                Adding new product
              </Typography>
            </Grid>
            <form onSubmit={handleSubmit}>
              <Grid
                display="flex"
                container
                sx={{ mt: 1, border: 0 }}
                spacing={2}
              >
                <Grid
                  container
                  justifyContent="center"
                  alignItems="center"
                  xs={12}
                  sx={{ mb: 4, pl: 1.5 }}
                >
                  <TextField
                    justifyContent="flex-end"
                    autoComplete="product_name"
                    name="Product's name"
                    variant="outlined"
                    required
                    id="product_name"
                    label="Product's name"
                    sx={{ width: 490 }}
                    onChange={(e) => setProduct_name(e.target.value)}
                    autoFocus
                  />
                </Grid>
                <Grid
                  container
                  justifyContent="center"
                  xs={12}
                  sx={{ mb: 2, pl: 1.5 }}
                >
                  <TextField
                    variant="outlined"
                    required
                    id="Product's picture"
                    label="Product's picture URL"
                    sx={{ width: 490 }}
                    onChange={(e) => setProduct_picture(e.target.value)}
                  />
                </Grid>
                <Grid item xs={12} sx={{ mb: 2, border: 0 }}>
                  <TextField
                    variant="outlined"
                    required
                    id="Description"
                    label="Detail"
                    sx={{ width: 490 }}
                    onChange={(e) => setDescription(e.target.value)}
                  />
                </Grid>
                <Grid item xs={12} sx={{ mb: 2 }}>
                  <TextField
                    variant="outlined"
                    required
                    id="Quantity"
                    label="Quantity"
                    sx={{ width: 490 }}
                    onChange={(e) => setQuantity(e.target.value)}
                  />
                </Grid>

                <Grid
                  item
                  xs={12}
                  sx={{ display: "flex", justifyContent: "center", pl: 1.5 }}
                >
                  <Button
                    type="submit"
                    variant="contained"
                    sx={{ width: 150, height: 50 }}
                  >
                    Submit
                  </Button>
                </Grid>
              </Grid>
            </form>
          </Box>
        </Grid>
      </Grid>
    </ThemeProvider>
  );
}
