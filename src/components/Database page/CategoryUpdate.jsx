import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import CssBaseline from "@mui/material/CssBaseline";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Avatar from "@mui/material/Avatar";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";

import { createTheme, ThemeProvider } from "@mui/material/styles";

import Swal from "sweetalert2";

import BorderColorIcon from "@mui/icons-material/BorderColor";

export default function CategoryUpdate() {
  const { id } = useParams();
  useEffect(() => {
    fetch("https://crud-wms.herokuapp.com/category/" + id)
      .then((res) => res.json())
      .then((result) => {
        setCategory_name(result.category_name);
        setImage(result.image);
      });
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault();
    var data = {
      category_id: id,
      category_name: category_name,
      image: image,
    };
    fetch("https://crud-wms.herokuapp.com/updatecategory", {
      method: "PUT",
      headers: {
        Accept: "application/form-data",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((result) => {
        const ok = JSON.stringify(result.status);
        if (ok == ok) {
          Swal.fire({
            icon: "success",
            title: "Updated!",
            text: result["message"],
            timer: 2000,
            timerProgressBar: true,
            showConfirmButton: false,
          });
          window.setTimeout(function () {
            window.location.href = "/Category";
          }, 2000);
        }
      })

      .catch((error) => console.log("error", error));
  };

  const [category_name, setCategory_name] = useState("");
  const [image, setImage] = useState("");

  const theme = createTheme({
    palette: {
      primary: {
        main: "#142454",
      },
    },
  });

  const avatarStyle = { backgroundColor: "#041444" };

  return (
    <ThemeProvider theme={theme}>
      <Grid container component="main" sx={{ height: "100vh" }}>
        <CssBaseline />
        <Grid
          item
          xs={false}
          sm={4}
          md={7}
          sx={{
            backgroundImage:
              "url(https://cdn.discordapp.com/attachments/744696824220221502/984030671427231775/unknown.png)",
            backgroundRepeat: "no-repeat",
            backgroundColor: (t) =>
              t.palette.mode === "light"
                ? t.palette.grey[50]
                : t.palette.grey[900],
            backgroundSize: "cover",
            backgroundPosition: "center",
          }}
        />
        <Grid
          item
          align="center"
          xs={12}
          sm={8}
          md={5}
          component={Paper}
          elevation={6}
          square
        >
          <Box
            sx={{
              my: 0,
              mx: 4,
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              marginTop: 10,
              border: 0,
              height: 750,
            }}
          >
            <Avatar style={avatarStyle} sx={{ height: 85, width: 85 }}>
              <BorderColorIcon sx={{ height: 50, width: 50 }} />
            </Avatar>
            <Typography component="h1" variant="h5" sx={{ mt: 3 }}>
              Edit category's detail
            </Typography>

            <form onSubmit={handleSubmit}>
              <Grid
                display="flex"
                container
                sx={{ mt: 1, mb: 1, border: 0 }}
                spacing={2}
              >
                <Grid
                  item
                  justifyContent="center"
                  alignItems="center"
                  xs={12}
                  sx={{ border: 0, mb: 1, pl: 2 }}
                >
                  <TextField
                    justifyContent="flex-end"
                    autoComplete="category_name"
                    name="Category's name"
                    variant="outlined"
                    required
                    id="Category's name"
                    label="Category's name"
                    value={category_name}
                    sx={{ width: 490 }}
                    onChange={(e) => setCategory_name(e.target.value)}
                    autoFocus
                  />
                </Grid>

                <Grid item xs={12} sx={{ border: 0, mb: 1 }}>
                  <TextField
                    variant="outlined"
                    required
                    id="image"
                    label="Categoty's picture URL"
                    value={image}
                    sx={{ width: 490 }}
                    onChange={(e) => setImage(e.target.value)}
                  />
                </Grid>

                <Grid
                  item
                  xs={12}
                  sx={{ display: "flex", justifyContent: "center" }}
                >
                  <Button
                    type="submit"
                    variant="contained"
                    sx={{ width: 150, height: 50 }}
                  >
                    Submit Update
                  </Button>
                </Grid>
              </Grid>
            </form>
          </Box>
        </Grid>
      </Grid>
    </ThemeProvider>
  );
}
