import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import CssBaseline from "@mui/material/CssBaseline";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Avatar from "@mui/material/Avatar";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import Select from "@mui/material/Select";

import { createTheme, ThemeProvider } from "@mui/material/styles";

import Swal from "sweetalert2";

import BorderColorIcon from "@mui/icons-material/BorderColor";

export default function UserUpdate() {
  const { id } = useParams();
  useEffect(() => {
    fetch("https://crud-wms.herokuapp.com/pd/" + id)
      .then((res) => res.json())
      .then((result) => {
        setProduct_name(result.product_name);
        setDescription(result.description);
        setProduct_picture(result.product_picture);
        setQuantity(result.Quantity);
        setCategory_id(result.category_id);
      });
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault();
    var data = {
      product_id: id,
      product_name: product_name,
      description: description,
      product_picture: product_picture,
      Quantity: Quantity,
      category_id: category_id,
    };
    fetch("https://crud-wms.herokuapp.com/updatepd", {
      method: "PUT",
      headers: {
        Accept: "application/form-data",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((result) => {
        const ok = JSON.stringify(result.status);
        if (ok == ok) {
          Swal.fire({
            icon: "success",
            title: "Updated!",
            text: result["message"],
            timer: 2000,
            timerProgressBar: true,
            showConfirmButton: false,
          });
          window.setTimeout(function () {
            window.location.href = "/Products";
          }, 2000);
        }
      })

      .catch((error) => console.log("error", error));
  };

  const [product_name, setProduct_name] = useState("");
  const [description, setDescription] = useState("");
  const [product_picture, setProduct_picture] = useState("");
  const [Quantity, setQuantity] = useState("");
  const [category_id, setCategory_id] = useState([]);
  const [category, setCategory] = useState([]);
  useEffect(() => {
    CategoryGet();
  }, []);

  const CategoryGet = () => {
    fetch("https://crud-wms.herokuapp.com/category")
      .then((res) => res.json())
      .then((rows) => {
        setCategory(rows);
      });
  };

  const theme = createTheme({
    palette: {
      primary: {
        main: "#142454",
      },
    },
  });

  const avatarStyle = { backgroundColor: "#041444" };

  return (
    <ThemeProvider theme={theme}>
      <Grid container component="main" sx={{ height: "100vh" }}>
        <CssBaseline />
        <Grid
          item
          xs={false}
          sm={4}
          md={7}
          sx={{
            backgroundImage:
              "url(https://cdn.discordapp.com/attachments/744696824220221502/984030671427231775/unknown.png)",
            backgroundRepeat: "no-repeat",
            backgroundColor: (t) =>
              t.palette.mode === "light"
                ? t.palette.grey[50]
                : t.palette.grey[900],
            backgroundSize: "cover",
            backgroundPosition: "center",
          }}
        />
        <Grid
          item
          align="center"
          xs={12}
          sm={8}
          md={5}
          component={Paper}
          elevation={6}
          square
        >
          <Box
            sx={{
              my: 0,
              mx: 4,
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              marginTop: 10,
              border: 0,
              height: 750,
            }}
          >
            <Avatar style={avatarStyle} sx={{ height: 85, width: 85 }}>
              <BorderColorIcon sx={{ height: 50, width: 50 }} />
            </Avatar>
            <Typography component="h1" variant="h5" sx={{ mt: 3 }}>
              Edit Product's detail
            </Typography>

            <form onSubmit={handleSubmit}>
              <Grid
                display="flex"
                container
                sx={{ mt: 1, mb: 1, border: 0 }}
                spacing={2}
              >
                <Grid
                  item
                  justifyContent="center"
                  alignItems="center"
                  xs={12}
                  sx={{ border: 0, mb: 1, pl: 2 }}
                >
                  <TextField
                    justifyContent="flex-end"
                    autoComplete="product_name"
                    name="Product's name"
                    variant="outlined"
                    required
                    id="Product's name"
                    label="Product's name"
                    value={product_name}
                    sx={{ width: 490 }}
                    onChange={(e) => setProduct_name(e.target.value)}
                    autoFocus
                  />
                </Grid>

                <Grid item xs={12} sx={{ border: 0, mb: 1 }}>
                  <TextField
                    variant="outlined"
                    required
                    id="product_picture"
                    label="product's picture URL"
                    value={product_picture}
                    sx={{ width: 490 }}
                    onChange={(e) => setProduct_picture(e.target.value)}
                  />
                </Grid>
                <Grid
                  item
                  justifyContent="center"
                  xs={12}
                  sx={{ mb: 1, pl: 2 }}
                >
                  <TextField
                    variant="outlined"
                    required
                    id="description"
                    label="description"
                    value={description}
                    sx={{ width: 490 }}
                    onChange={(e) => setDescription(e.target.value)}
                  />
                </Grid>
                <Grid item xs={12} sx={{ mb: 2 }}>
                  <TextField
                    variant="outlined"
                    required
                    id="Quantity"
                    label="Quantity"
                    value={Quantity}
                    sx={{ width: 490 }}
                    onChange={(e) => setQuantity(e.target.value)}
                  />
                </Grid>

                <Grid
                  item
                  justifyContent="center"
                  xs={12}
                  sx={{ mb: 1, pl: 2 }}
                >
                  <FormControl style={{ width: 490 }}>
                    <InputLabel id="demo-simple-select-label">
                      Please select a category
                    </InputLabel>

                    <Select
                      variant="outlined"
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      label="Please select a category"
                      onChange={(e) => setCategory_id(e.target.value)}
                    >
                      <MenuItem value={null}>RESET</MenuItem>
                      {category.map((category) => (
                        <MenuItem
                          key={category.category_id}
                          value={category.category_id}
                        >
                          {category.category_name}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                </Grid>

                <Grid
                  item
                  xs={12}
                  sx={{ display: "flex", justifyContent: "center" }}
                >
                  <Button
                    type="submit"
                    variant="contained"
                    sx={{ width: 150, height: 50 }}
                  >
                    Submit Update
                  </Button>
                </Grid>
              </Grid>
            </form>
          </Box>
        </Grid>
      </Grid>
    </ThemeProvider>
  );
}
