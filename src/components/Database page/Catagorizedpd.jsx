import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Avatar from "@mui/material/Avatar";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { styled } from "@mui/material/styles";
import PropTypes from "prop-types";
import TextField from "@mui/material/TextField";
import IconButton from "@mui/material/IconButton";
import TableFooter from "@mui/material/TableFooter";
import TablePagination from "@mui/material/TablePagination";
import { createTheme, useTheme, ThemeProvider } from "@mui/material/styles";

import Appbar from "../appbar";

import ClearIcon from "@mui/icons-material/Clear";
import SearchIcon from "@mui/icons-material/Search";
import FirstPageIcon from "@mui/icons-material/FirstPage";
import KeyboardArrowLeft from "@mui/icons-material/KeyboardArrowLeft";
import KeyboardArrowRight from "@mui/icons-material/KeyboardArrowRight";
import LastPageIcon from "@mui/icons-material/LastPage";
import { CategoryScale } from "chart.js";

function TablePaginationActions(props) {
  const theme = useTheme();
  const { count, page, rowsPerPage, onPageChange } = props;

  const handleFirstPageButtonClick = (event) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <Box sx={{ flexShrink: 0, ml: 2.5, border: 0 }}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowRight />
        ) : (
          <KeyboardArrowLeft />
        )}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowLeft />
        ) : (
          <KeyboardArrowRight />
        )}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </Box>
  );
}

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};

export default function Products() {
  const [platform, setPlatform] = useState([]);
  const [searchText, setSearchText] = useState("");
  const [rows] = useState([]);
  const [products, setProduct] = useState([]);

  const { id } = useParams();
  useEffect(() => {
    fetch("https://crud-wms.herokuapp.com/categorizedpd/" + id)
      .then((res) => res.json())
      .then((rows) => {
        setProduct(rows);
        setPlatform(rows);
      });
  }, []);

  function escapeRegExp(value) {
    return value.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
  }

  const requestSearch = (searchValue) => {
    const searchRegex = new RegExp(escapeRegExp(searchValue), "i");
    const filteredProducts = platform.filter((products) => {
      return Object.keys(products).some((field) => {
        return searchRegex.test(products[field]);
      });
    });
    setProduct(filteredProducts);
  };

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const mdTheme = createTheme({
    palette: {
      primary: {
        main: "#001345",
        info: "#e7e8fc",
      },
      secondary: {
        main: "#878787",
        info: "#ffffff",
      },
      neutral: {
        main: "#ffffff",
        contrastText: "#fff",
      },
    },
  });

  const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.primary.main,
      color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 18,
    },
  }));

  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    "&:last-child td, &:last-child th": {
      border: 0,
    },
  }));

  return (
    <ThemeProvider theme={mdTheme}>
      <Grid container spacing={1}>
        <Box
          sx={{
            border: 0,
            bgcolor: "primary.info",
            width: "100%",
            height: "auto",
            minHeight: 890,
            borderLeft: 71,
            borderColor: "primary.main",
          }}
        >
          <Grid item xs={12}>
            <Appbar />
            <Grid>
              <Paper sx={{ p: 3, m: 5, mt: 15 }}>
                <Box display="flex">
                  <Box flexGrow={1}>
                    <Typography
                      component="h2"
                      variant="h6"
                      color="primary.main"
                      gutterBottom
                      sx={{ ml: 3, mt: 1 }}
                    >
                      Products
                    </Typography>
                  </Box>

                  <Box
                    display="flex"
                    sx={{ justifyContent: "flex-end", mr: 5 }}
                  >
                    <TextField
                      variant="standard"
                      value={searchText}
                      onChange={(e) => {
                        setSearchText(e.target.value);
                        requestSearch(e.target.value);
                      }}
                      placeholder="Search..."
                      InputProps={{
                        startAdornment: (
                          <SearchIcon fontSize="medium" color="action" />
                        ),
                        endAdornment: (
                          <IconButton
                            title="Clear"
                            aria-label="Clear"
                            size="small"
                            style={{
                              visibility: searchText ? "visible" : "hidden",
                              borderRadius: "57%",
                              paddingRight: "1px",
                              margin: "0",
                              fontSize: "1.25rem",
                            }}
                            onClick={(e) => {
                              setSearchText("");
                              setProduct(platform);
                            }}
                          >
                            <ClearIcon fontSize="medium" color="action" />
                          </IconButton>
                        ),
                      }}
                      sx={{
                        width: { xs: 1, sm: "auto" },
                        m: (theme) => theme.spacing(1, 0.5, 1.5),
                        "& .MuiSvgIcon-root": {
                          mr: 0.5,
                        },
                        "& .MuiInput-underline:before": {
                          borderBottom: 1,
                          borderColor: "divider",
                        },
                      }}
                    />
                  </Box>
                </Box>
                <TableContainer component={Paper} sx={{ mt: 2 }}>
                  <Table aria-label="customized table">
                    <TableHead>
                      <StyledTableRow>
                        <StyledTableCell align="center" width="10%">
                          ID
                        </StyledTableCell>
                        <StyledTableCell align="center" width="10%">
                          Products's Name
                        </StyledTableCell>
                        <StyledTableCell align="center">
                          Picture
                        </StyledTableCell>
                        <StyledTableCell align="left" width="30%">
                          Description
                        </StyledTableCell>
                        <StyledTableCell align="center" width="10%">
                          Quantity
                        </StyledTableCell>
                      </StyledTableRow>
                    </TableHead>
                    <TableBody>
                      {(rowsPerPage > 0
                        ? products.slice(
                            page * rowsPerPage,
                            page * rowsPerPage + rowsPerPage
                          )
                        : products
                      ).map((product) => (
                        <StyledTableRow key={product.product_id}>
                          <StyledTableCell align="center">
                            {product.product_id}
                          </StyledTableCell>

                          <StyledTableCell align="left">
                            {product.product_name}
                          </StyledTableCell>
                          <StyledTableCell align="center">
                            <Box display="flex" justifyContent="center">
                              <Avatar
                                src={product.product_picture}
                                sx={{ width: 150, height: 150 }}
                              />
                            </Box>
                          </StyledTableCell>
                          <StyledTableCell align="left">
                            {product.description}
                          </StyledTableCell>
                          <StyledTableCell align="center">
                            {product.Quantity}
                          </StyledTableCell>
                        </StyledTableRow>
                      ))}
                    </TableBody>
                    <TableFooter>
                      <TableRow>
                        <TablePagination
                          rowsPerPageOptions={[
                            10,
                            25,
                            50,
                            100,
                            { label: "All", value: -1 },
                          ]}
                          colSpan={4}
                          count={products.length}
                          rowsPerPage={rowsPerPage}
                          page={page}
                          SelectProps={{
                            inputProps: {
                              "aria-label": "rows per page",
                            },
                            native: true,
                          }}
                          onPageChange={handleChangePage}
                          onRowsPerPageChange={handleChangeRowsPerPage}
                          ActionsComponent={TablePaginationActions}
                        />
                      </TableRow>
                    </TableFooter>
                  </Table>
                </TableContainer>
              </Paper>
            </Grid>
          </Grid>
        </Box>
      </Grid>
    </ThemeProvider>
  );
}
