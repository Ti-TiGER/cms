import * as React from "react";
import Grid from "@mui/material/Grid";
import ToggleButton from "@mui/material/ToggleButton";
import ToggleButtonGroup from "@mui/material/ToggleButtonGroup";

//Chart thingy
import { Chart, ArcElement } from "chart.js";
import { Pie } from "react-chartjs-2";
Chart.register(ArcElement);

function preventDefault(event) {
  event.preventDefault();
}

const data = {
  labels: ["January", "February", "March", "April", "May"],
  datasets: [
    {
      label: "Rainfall",
      backgroundColor: ["#ea8bcb", "#fff59d", "#001345", "#c5a0ff", "#def9ff"],
      hoverBackgroundColor: [
        "#754666",
        "#807b4f",
        "#000a23",
        "#635080",
        "#6f7d80",
      ],
      data: [100, 200, 300, 400, 500],
    },
  ],
};

export default function Exported() {
  const [alignment, setAlignment] = React.useState("Week");

  const handleAlignment = (event, newAlignment) => {
    if (newAlignment !== null) {
      setAlignment(newAlignment);
    }
  };
  return (
    <React.Fragment>
      <Grid container spacing={2} sx={{ border: 0 }}>
        <Grid item xs={8} sx={{ border: 0, maxHeight: 300, mt: 2 }}>
          <Pie data={data} style={{ maxWidth: "95%", maxHeight: "95%" }} />
        </Grid>

        <Grid item xs={4} sx={{ border: 0 }}>
          <ToggleButtonGroup
            sx={{ ml: 2.25, mt: 2 }}
            color="primary"
            orientation="vertical"
            value={alignment}
            exclusive
            onChange={handleAlignment}
          >
            <ToggleButton value="Week">Week</ToggleButton>
            <ToggleButton value="Month">Month</ToggleButton>
            <ToggleButton value="Year">Year</ToggleButton>
          </ToggleButtonGroup>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}
